#include <stdlib.h>
#include "list.h"
#include "contact.h"

void deleteNodesForward(ListNode*);

void deleteNodesForward(ListNode* node) {
    if (node == NULL)
        return;
    if (node->contact != NULL)
        freeContact(node->contact);
    ListNode* next = node->next;
    free(node);
    deleteNodesForward(next);
}

ListNode* newListNode(Contact* contact) {
    ListNode* node = (ListNode*) malloc(sizeof(ListNode));
    node->next = NULL;
    node->prev = NULL;
    node->contact = contact;
    return node;
}

List* newList() {
    List* list = (List*) malloc(sizeof(List));
    ListNode* head_guard = newListNode(NULL);
    ListNode* tail_guard = newListNode(NULL);
    list->head = head_guard;
        head_guard->next = tail_guard;
    list->tail = tail_guard;
        tail_guard->prev = head_guard;
    return list;
}

void deleteList(List* list) {
    if (list == NULL)
        return;
    ListNode* node = list->head;
    deleteNodesForward(node);
    free(list);
}

void appendContactToList(List* list, Contact* contact) {
    ListNode* node = newListNode(contact);
    list->tail->prev->next = node;
    node->prev = list->tail->prev;
    node->next = list->tail;
    list->tail->prev = node;
}

ListNode* findContactNode(ListNode* node, Contact* contact) {
    if (node == NULL) 
        return NULL;

    return (node->contact == contact) ? node : findContactNode(node->next, contact);
}

void deleteContactFromList(List* list, Contact* contact) {
    ListNode* node = findContactNode(list->head, contact);
    if (node == NULL)
        return;
    node->prev->next = node->next;
    node->next->prev = node->prev;
    freeContact(node->contact);
    free(node);
}

void splitList(List* list, List* smallerElements, List* equalElements, List* greaterElements, Ordering (*comparator)(Contact*, Contact*)) {
    ListNode* ptr = list->head->next;
    ListNode* nextNode;
    Contact* comparisonBase = ptr->contact;
    ListNode* smallerPtr = smallerElements->head;
    ListNode* equalPtr = equalElements->head;
    ListNode* greaterPtr = greaterElements->head;

    while (ptr->next != NULL) {
        nextNode = ptr->next;
        switch ((*comparator)(ptr->contact, comparisonBase)) {
            case LT:
                ptr->next = smallerPtr->next;
                ptr->prev = smallerPtr;
                smallerPtr->next->prev = ptr;
                smallerPtr->next = ptr;
                smallerPtr = ptr;
                break;
            case EQ:
                ptr->next = equalPtr->next;
                ptr->prev = equalPtr;
                equalPtr->next->prev = ptr;
                equalPtr->next = ptr;
                equalPtr = ptr;
                break;
            case GT:
                ptr->next = greaterPtr->next;
                ptr->prev = greaterPtr;
                greaterPtr->next->prev = ptr;
                greaterPtr->next = ptr;
                greaterPtr = ptr;
                break;
        }
        ptr = nextNode;
    }
}

void mergeLists(List* targetList, List* list1, List* list2, List* list3) {
            free(targetList->head);
            free(targetList->tail);
    targetList->head = list1->head;
    list1->tail->prev->next = list2->head->next;
    list2->head->next->prev = list1->tail->prev;
            free(list1->tail);
    list2->tail->prev->next = list3->head->next;
    list3->head->next->prev = list2->tail->prev;
            free(list2->tail);
            free(list3->head);
    targetList->tail = list3->tail;

    free(list1);
    free(list2);
    free(list3);
}

void sortList(List* list, Ordering (*comparator)(Contact*, Contact*)) {
    if(list->head->next == list->tail->prev || list->head->next == list->tail)
        return;

    List* smallerElements = newList();
    List* equalElements = newList();
    List* greaterElements = newList();

    splitList(list, smallerElements, equalElements, greaterElements, comparator);

    sortList(smallerElements, *comparator);
    sortList(greaterElements, *comparator);

    mergeLists(list, smallerElements, equalElements, greaterElements);
}

Contact* findContactInList(List* list, char* firstName, char* lastName, char* email, char* phoneNumber) {
    ListNode* ptr = list->head->next;
    while (ptr->next != NULL && ptr->contact != NULL) {
        if (contactMatches(ptr->contact, firstName, lastName, email, phoneNumber))
            return ptr->contact;
        ptr = ptr->next;
    }
    return NULL;
}

void printContactNodes(ListNode* node) {
    if (node == NULL)
        return;
    if (node->contact != NULL)
        printContact(node->contact);
    printContactNodes(node->next);
}

void printContactList(List* list) {
    printContactNodes(list->head);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "contact.h"

Contact* contactFromString(char* string) {
    /*
     * input string format:
     * name|surname|birth_date|email|phone_number|address
     */
    Contact* contact = (Contact*) malloc(sizeof(Contact));

    char* ch;
    ch = strtok(string, "|");
        contact->firstName = (char*) malloc(strlen(ch)+1);
        strcpy(contact->firstName, ch);
    ch = strtok(NULL, "|");
        contact->lastName = (char*) malloc(strlen(ch)+1);
        strcpy(contact->lastName, ch);
    ch = strtok(NULL, "|");
        contact->birthDate = atoi(ch);
    ch = strtok(NULL, "|");
        contact->email = (char*) malloc(strlen(ch)+1);
        strcpy(contact->email, ch);
    ch = strtok(NULL, "|");
        contact->phoneNumber = (char*) malloc(strlen(ch)+1);
        strcpy(contact->phoneNumber, ch);
    ch = strtok(NULL, "|");
        contact->address = (char*) malloc(strlen(ch)+1);
        strcpy(contact->address, ch);

    return contact;
}

Contact* newContact(char* firstName, char* lastName, time_t birthDate, char* email, char* phoneNumber, char* address) {
    Contact* contact = (Contact*) malloc(sizeof(Contact));
    contact->firstName = (char*) malloc(sizeof(firstName));
        strcpy(contact->firstName, firstName);
    contact->lastName = (char*) malloc(sizeof(lastName));
        strcpy(contact->lastName, lastName);
    contact->birthDate = birthDate;
    contact->email = (char*) malloc(sizeof(email));
        strcpy(contact->email, email);
    contact->phoneNumber = (char*) malloc(sizeof(email));
        strcpy(contact->phoneNumber, email);
    contact->address = (char*) malloc(sizeof(address));
        strcpy(contact->address, address);
    return contact;
}

void freeContact(Contact* contact) {
    if(contact == NULL)
        return;
    if (contact->firstName != NULL)
        free(contact->firstName);
    if (contact->lastName != NULL)
        free(contact->lastName);
    if (contact->email != NULL)
        free(contact->email);
    if (contact->phoneNumber != NULL)
        free(contact->phoneNumber);
    if (contact->address != NULL)
        free(contact->address);
    free(contact);
}

Ordering nullContactComparator(Contact* contact1, Contact* contact2) {
    if (contact1 == NULL && contact2 == NULL)
        return EQ;
    if (contact1 == NULL)
        return LT;
    if (contact2 == NULL)
        return GT;
}

Ordering compareByLastName(Contact* contact1, Contact* contact2) {
    if (contact1 != NULL && contact2 != NULL)
        return toOrdering(strcmp(contact1->lastName, contact2->lastName));
    return nullContactComparator(contact1, contact2);
}
Ordering compareByBirthDate(Contact* contact1, Contact* contact2) {
    if (contact1 != NULL && contact2 != NULL)
        return toOrdering(contact1->birthDate - contact2->birthDate);
    return nullContactComparator(contact1, contact2);
}
Ordering compareByEmail(Contact* contact1, Contact* contact2) {
    if (contact1 != NULL && contact2 != NULL)
        return toOrdering(strcmp(contact1->email, contact2->email));
    return nullContactComparator(contact1, contact2);
}
Ordering compareByPhoneNumber(Contact* contact1, Contact* contact2) {
    if (contact1 != NULL && contact2 != NULL)
        return toOrdering(strcmp(contact1->phoneNumber, contact2->phoneNumber));
    return nullContactComparator(contact1, contact2);
}

int contactMatches(Contact* contact, char* firstName, char* lastName, char* email, char* phoneNumber) {
    int matches = 1;
    if (firstName != NULL && strcmp(contact->firstName, firstName) ||
        lastName != NULL && strcmp(contact->lastName, lastName) ||
        email != NULL && strcmp(contact->email, email) ||
        phoneNumber != NULL && strcmp(contact->phoneNumber, phoneNumber) ||
        firstName == NULL && lastName == NULL && email == NULL && phoneNumber == NULL)
        matches = 0;
    return matches;
}

void printContact(Contact* contact) {
    if (contact != NULL)
        printf("%s\t%s\t\t%s\t\t%s\n", contact->firstName, contact->lastName, contact->phoneNumber, contact->email);
    else
        printf("no contact\n");
}

#include <string.h>
#include "types.h"

Ordering toOrdering(int difference) {
    if (difference < 0)
        return LT;
    if (difference > 0)
        return GT;
    return EQ;
}

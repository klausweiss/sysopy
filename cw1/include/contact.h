#include <time.h>
#include "types.h"

Contact*    contactFromString(char*);
Contact*    newContact(char*, char*, time_t, char*, char*, char*);
void        freeContact(Contact*);
int         contactMatches(Contact*, char*, char*, char*, char*);

Ordering    compareByLastName(Contact*, Contact*);
Ordering    compareByBirthDate(Contact*, Contact*);
Ordering    compareByEmail(Contact*, Contact*);
Ordering    compareByPhoneNumber(Contact*, Contact*);

void        printContact(Contact*);

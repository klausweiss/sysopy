#include "types.h"

#ifndef _list_lib
#define _list_lib

List*    newList();
void     deleteList(List*);
void     appendContactToList(List*, Contact*);
void     deleteContactFromList(List*, Contact*);
void     sortList(List*, Ordering (*comparator)(Contact*, Contact*));
Contact* findContactInList(List*, char*, char*, char*, char*);

void    printContactList(List*);

#endif

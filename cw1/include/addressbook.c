#include <stdlib.h>
#include "addressbook.h"
#include "contact.h"
#include "list.h"
#include "tree.h"
#include "types.h"

AddressBook* newAddressBook(AddressBookType type) {
    AddressBook* book = (AddressBook*) malloc(sizeof(AddressBook));
    switch (type) {
        case LIST:
            book->list = newList();
            book->tree = NULL;
            break;
        case TREE:
            book->list = NULL;
            book->tree = newTree();
    }
    return book;
}

void deleteAddressBook(AddressBook* book) {
    deleteList(book->list);
    deleteTree(book->tree);
    free(book);
}

void addContact(AddressBook* book, Contact* contact) {
    if (book->list != NULL)
        appendContactToList(book->list, contact);
    else if (book->tree != NULL)
        addContactToTree(book->tree, contact);
}

void deleteContactFromAddressBook(AddressBook* book, Contact* contact) {
    if (book->list != NULL)
        deleteContactFromList(book->list, contact);
    else if (book->tree != NULL)
        deleteContactFromTree(book->tree, contact);
}

void sortAddressBook(AddressBook* book, Ordering (*comparator)(Contact*, Contact*)) {
    if (book->list != NULL)
        sortList(book->list, *comparator);
    else if (book->tree != NULL)
        sortTree(book->tree, *comparator);
}

void printAddressBook(AddressBook* book) {
    if (book->list != NULL)
        printContactList(book->list);
    else if (book->tree != NULL)
        printContactTree(book->tree);
}

Contact* findContact(AddressBook* book, char* firstName, char* lastName, char* email, char* phoneNumber) {
    if (book->list != NULL)
        return findContactInList(book->list, firstName, lastName, email, phoneNumber);
    else if (book->tree != NULL)
        return findContactInTree(book->tree, firstName, lastName, email, phoneNumber);
}

void findAndDeleteContact(AddressBook* book, char* firstName, char* lastName, char* email, char* phoneNumber) {
    Contact* contact = findContact(book, firstName, lastName, email, phoneNumber);
    if (contact != NULL)
        deleteContactFromAddressBook(book, contact);
}

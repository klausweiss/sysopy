#include <stdlib.h>
#include "contact.h"
#include "tree.h"
#include "types.h"

void deleteNodes(TreeNode*);

void deleteNodes(TreeNode* node) {
    if (node == NULL)
        return;
    TreeNode* left = node->left;
    TreeNode* right = node->right;
    if (node->contact != NULL)
        freeContact(node->contact);
    free(node);
    deleteNodes(left);
    deleteNodes(right);
}

void deleteNodesWithoutContacts(TreeNode* node) {
    if (node == NULL)
        return;
    TreeNode* left = node->left;
    TreeNode* right = node->right;
    free(node);
    deleteNodesWithoutContacts(left);
    deleteNodesWithoutContacts(right);
}

TreeNode* newTreeNode(TreeNode* parent, Contact* contact) {
    TreeNode* node = (TreeNode*) malloc(sizeof(TreeNode));
        node->right = NULL;
        node->left = NULL;
        node->parent = parent;
        node->contact = contact;
    return node;
}

Tree* newTree() {
    Tree* tree = (Tree*) malloc(sizeof(Tree));
    TreeNode* root = newTreeNode(NULL, NULL);
    tree->root = root;
    tree->comparator = compareByLastName;
    return tree;
}

void deleteTree(Tree* tree) {
    if (tree == NULL)
        return;
    TreeNode* root = tree->root;
    deleteNodes(root);
    free(tree);
}

void addContactToTreeNode(Contact* contact, TreeNode* node, Ordering (*comparator)(Contact*, Contact*)) {
    Ordering ord = (*comparator)(contact, node->contact);
    switch (ord) {
        case LT:
            if (node->left != NULL)
                addContactToTreeNode(contact, node->left, *comparator);
            else
                node->left = newTreeNode(node, contact);
            break;
        default:
            if (node->right != NULL)
                addContactToTreeNode(contact, node->right, *comparator);
            else
                node->right = newTreeNode(node, contact);
            break;
    }
}

void addContactToTree(Tree* tree, Contact* contact) {
    addContactToTreeNode(contact, tree->root, tree->comparator);
}

TreeNode* findContactTreeNode(Contact* contact, TreeNode* node, Ordering (*comparator)(Contact*, Contact*)) {
    if (node == NULL || contact == NULL)
        return NULL;

    Ordering ord = (*comparator)(contact, node->contact);
    if (contact == node->contact)
        return node;

    switch (ord) {
        case LT:
            return findContactTreeNode(contact, node->left, *comparator);
        default:
            return findContactTreeNode(contact, node->right, *comparator);
    }
    return NULL;
}

void deleteLeaf(TreeNode* node) {
    if (node == node->parent->left)
        node->parent->left = NULL;
    else if (node == node->parent->right)
        node->parent->right = NULL;
    deleteNodes(node);
}

void deleteNodeWithLeftChild(TreeNode* node) {
    if (node == node->parent->left)
        node->parent->left = node->left;
    else if (node == node->parent->right)
        node->parent->right = node->left;
    node->left->parent = node->parent;
    node->left = NULL;
    deleteNodes(node);
}

void deleteNodeWithRightChild(TreeNode* node) {
    if (node == node->parent->left)
        node->parent->left = node->right;
    else if (node == node->parent->right)
        node->parent->right = node->right;
    node->right->parent = node->parent;
    node->right = NULL;
    deleteNodes(node);
}

TreeNode* findSuccessorInChildren(TreeNode* node, Ordering (*comparator)(Contact*, Contact*)) {
    TreeNode* ptr = node->right;
    while (ptr->left != NULL)
        ptr = ptr->left;
    return ptr;
}

void deleteNodeWithBothChildren(TreeNode* node, Ordering (*comparator)(Contact*, Contact*)) {
    TreeNode* successor = findSuccessorInChildren(node, *comparator);
    freeContact(node->contact);
    node->contact = successor->contact;
    if (successor->right != NULL) {
        successor->parent->left = successor->right;
        successor->right->parent = successor->parent;
        free(successor);
    }
}

void deleteContactFromTree(Tree* tree, Contact* contact) {
    TreeNode* node = findContactTreeNode(contact, tree->root, *(tree->comparator));
    if (node == NULL)
        return;

    if (node->left == NULL && node->right == NULL)
        deleteLeaf(node);
    else if(node->left == NULL)
        deleteNodeWithRightChild(node);
    else if(node->right == NULL)
        deleteNodeWithLeftChild(node);
    else
        deleteNodeWithBothChildren(node, *tree->comparator);
}

void printSubTree(TreeNode* node) {
    if (node == NULL)
        return;

    printSubTree(node->left);
    if (node->contact != NULL)
        printContact(node->contact);
    printSubTree(node->right);
}

void printContactTree(Tree* tree) {
    if (tree != NULL)
        printSubTree(tree->root);
}

void copyNodes(TreeNode* node, Tree* tree) {
    if(node == NULL)
        return;
    addContactToTree(tree, node->contact);
    copyNodes(node->left, tree);
    copyNodes(node->right, tree);
}

void moveTree(Tree* source, Tree* dest) {
    copyNodes(source->root->right, dest);
    deleteNodesWithoutContacts(source->root);
}

void sortTree(Tree* tree, Ordering (*comparator)(Contact*, Contact*)) {
    Tree* createdTree = newTree();
    createdTree->comparator = *comparator;

    moveTree(tree, createdTree);
    tree->root = createdTree->root;
    tree->comparator = *comparator;
    free(createdTree);
}

Contact* findContactInTreeNode(TreeNode* node, char* firstName, char* lastName, char* email, char* phoneNumber) {
    if (node == NULL || node->contact == NULL)
        return NULL;
    if (contactMatches(node->contact, firstName, lastName, email, phoneNumber))
        return node->contact;
    // this should be done in O(logn) based on the current tree comparator
    Contact* leftChildContact = findContactInTreeNode(node->left, firstName, lastName, email, phoneNumber);
    if (leftChildContact != NULL)
        return leftChildContact;
    return findContactInTreeNode(node->right, firstName, lastName, email, phoneNumber);
}

Contact* findContactInTree(Tree* tree, char* firstName, char* lastName, char* email, char* phoneNumber) {
    findContactInTreeNode(tree->root->right, firstName, lastName, email, phoneNumber);
}


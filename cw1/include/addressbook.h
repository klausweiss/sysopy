#include "types.h"

#ifndef _addressbook_lib
#define _addressbook_lib

AddressBook*    newAddressBook(AddressBookType);
void            deleteAddressBook(AddressBook*);
void            addContact(AddressBook*, Contact*);
void            sortAddressBook(AddressBook*, Ordering (*comparator)(Contact*, Contact*));
void            deleteContactFromAddressBook(AddressBook*, Contact*);
Contact*        findContact(AddressBook*, char*, char*, char*, char*);
void            findAndDeleteContact(AddressBook*, char*, char*, char*, char*);

void            printAddressBook(AddressBook*);

#endif


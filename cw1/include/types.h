#include <time.h>

#ifndef _addressbook_types
#define _addressbook_types

typedef int Ordering;
#define GT 1
#define LT -1
#define EQ 2
Ordering toOrdering(int);

typedef struct {
    char*   firstName;
    char*   lastName;
    time_t  birthDate;
    char*   email;
    char*   phoneNumber;
    char*   address;
} Contact;

typedef struct ListNode {
    struct ListNode*    prev;
    struct ListNode*    next;
    Contact*            contact;
} ListNode;

typedef struct {
    ListNode*   head;
    ListNode*   tail;
} List;

typedef struct TreeNode {
    struct TreeNode*    right;
    struct TreeNode*    left;
    struct TreeNode*    parent;
    Contact*            contact;
} TreeNode;

typedef struct {
    TreeNode*   root;
    Ordering    (*comparator)(Contact*, Contact*);
} Tree;

typedef int AddressBookType;
#define LIST 1
#define TREE 2
typedef struct {
    Tree*   tree;
    List*   list;
} AddressBook;

#endif

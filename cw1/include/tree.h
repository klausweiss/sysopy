#include "types.h"

#ifndef _tree_lib
#define _tree_lib

Tree*    newTree();
void     deleteTree(Tree*);
void     addContactToTree(Tree*, Contact*);
void     deleteContactFromTree(Tree*, Contact*);
void     sortTree(Tree*, Ordering (*comparator)(Contact*, Contact*));
Contact* findContactInTree(Tree*, char*, char*, char*, char*);

void    printContactTree(Tree*);
#endif

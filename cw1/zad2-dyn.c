#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/resource.h>
#include <dlfcn.h>
#include "include/types.h"

void readDataFromFile(AddressBook*);
void testBook(AddressBook*, char*);
void resetTimer();
void measureTime(char*);
void loadLibraryFunctions();

struct rusage startRusage;
struct rusage endRusage;
struct timespec startReal;
struct timespec endReal;

AddressBook* (*newAddressBook)(AddressBookType);
void (*deleteAddressBook)(AddressBook* book);
void (*sortAddressBook)(AddressBook*, Ordering (*comparator)(Contact*, Contact*));
Contact* (*findContact)(AddressBook*, char*, char*, char*, char*);
void (*findAndDeleteContact)(AddressBook*, char*, char*, char*, char*);
Contact* (*newContact)(char*, char*, time_t, char*, char*, char*);
Contact* (*contactFromString)(char*);
Ordering (*compareByLastName)(Contact*, Contact*);
Ordering (*compareByEmail)(Contact*, Contact*);
void (*addContact)(AddressBook*, Contact*);

int main(int argc, char **argv) {
    loadLibraryFunctions();

    resetTimer();
    AddressBook* listBook = newAddressBook(LIST);
    AddressBook* treeBook = newAddressBook(TREE);

    testBook(listBook, "LIST-BASED ADDRESSBOOK");
    testBook(treeBook, "TREE-BASED ADDRESSBOOK");

    deleteAddressBook(listBook);
    deleteAddressBook(treeBook);
    return 0;
}

void loadLibraryFunctions() {
    void* libaddressbook = dlopen("lib/libaddressbook.so", RTLD_LAZY);
    newAddressBook = dlsym(libaddressbook, "newAddressBook");
    deleteAddressBook = dlsym(libaddressbook, "deleteAddressBook");
    sortAddressBook = dlsym(libaddressbook, "sortAddressBook");
    findContact = dlsym(libaddressbook, "findContact");
    addContact = dlsym(libaddressbook, "addContact");
    findAndDeleteContact = dlsym(libaddressbook, "findAndDeleteContact");
    newContact = dlsym(libaddressbook, "newContact");
    contactFromString = dlsym(libaddressbook, "contactFromString");
    compareByLastName = dlsym(libaddressbook, "compareByLastName");
    compareByEmail= dlsym(libaddressbook, "compareByEmail");
}

void testBook(AddressBook* book, char* description) {
    printf("%s\n", description);

    readDataFromFile(book);
    measureTime("create (loading from file)");

    sortAddressBook(book, compareByLastName);
    measureTime("sort (last name)");

    sortAddressBook(book, compareByEmail);
    measureTime("sort (email)");

    Contact* olegWise = findContact(book, "Oleg", "Wise", NULL, NULL);
    measureTime("find");

    Contact* edwardAdkins = findContact(book, "Edward", "Adkins", NULL, NULL);
    measureTime("find");

    findAndDeleteContact(book, "Oleg", "Wise", NULL, NULL);
    measureTime("delete");

    findAndDeleteContact(book, "Edward", "Adkins", NULL, NULL);
    measureTime("delete");

    Contact* contact = newContact("Abraham", "Lincoln", 1489491834, "abraham@gov.us", "+1 123 123 123123", "White House");
    addContact(book, contact);
    measureTime("add contact");

    // assert that contacts are deleted from the addressbook
    assert(findContact(book, "Oleg", "Wise", NULL, NULL) == NULL);
    assert(findContact(book, "Edward", "Adkins", NULL, NULL) == NULL);
}

void resetTimer() {
    getrusage(RUSAGE_SELF, &startRusage);
    clock_gettime(CLOCK_MONOTONIC_RAW, &startReal);
}

void measureTime(char* description) {
    getrusage(RUSAGE_SELF, &endRusage);
    clock_gettime(CLOCK_MONOTONIC_RAW, &endReal);
    printf("\tUSER:\t %10ldus\t",     (long) ((endRusage.ru_utime.tv_sec - startRusage.ru_utime.tv_sec) * 1000000 + (endRusage.ru_utime.tv_usec - startRusage.ru_utime.tv_usec)));
    printf("\tSYSTEM:\t %10ldus\t",   (long) ((endRusage.ru_stime.tv_sec - startRusage.ru_stime.tv_sec) * 1000000 + (endRusage.ru_stime.tv_usec - startRusage.ru_stime.tv_usec)));
    printf("REAL:\t %10ldus  ",       (long) ((endReal.tv_sec - startReal.tv_sec) * 1000000000 + (endReal.tv_nsec - startReal.tv_nsec)) / 1000);
    printf("|--- %s\n", description);
    resetTimer();
}

void readDataFromFile(AddressBook* book) {
    FILE* dataFile;
    char buffer[256];

    if ((dataFile = fopen("data.csv", "r")) == NULL) {
        printf("Error while opening `data.csv` file");
        exit(1);
    }

    while(fgets(buffer, sizeof(buffer), dataFile)) {
        Contact* contact = contactFromString(buffer);
        addContact(book, contact);
    }
    fclose(dataFile);
}

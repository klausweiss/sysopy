# uhttp

Wielowątkowy serwer HTTP zaimplementowany w języku C. Obsługuje metody GET oraz POST. Loguje swoją aktywność do plików `.log`.

## zastosowane mechanizmy, opis procesów, zależności, kanałów, modułów

W celu obsługi wielu zapytań jednocześnie użyłem wątków (pthread). Wątki nie blokują się na żadnych zasobach, dzieki czemu każdy klient rozpatrywany jest indywidualnie.
Aby umożliwić logowanie aktywności serwera bez konieczności blokowania się do zapisu na plikach `.log` serwer uruchamia się na dwóch procesach - jednym obsługującym zapytania HTTP oraz drugim logującym aktywność pierwszego - każdy stan, który ma być logowany wysyłany jest z procesu sewera do procesu loggera kolejką komunikatów - dopóki w kolejce znajdują się jakieś wiadomości, logger je odbiera oraz zapisuje do odpowiednich plików (zwykłe logi, logi błędów).
W celu wyłączania serwera przechwytuję sygnał SIGINT.

## uruchamianie

Do uruchomienia serwera potrzebny jest konfiguracyjny plik `.ini`. Plik ten powinien posiadać jedną sekcję - `[server]`, a w niej pola `root`, `oklog`, `errlog`, `order` oraz `port`. Opisy tych pól wyświetlane są po uruchomieniu skompilowanego programu: `./server`.

Kompilacja oraz właściwe uruchamianie:

    cmake .
    make
    ./server config.ini

## autor

Mikołaj Biel, II rok

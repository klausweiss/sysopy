#include <ctype.h>
#include <netinet/ip.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>

#define true 1
#define false 0
#define min(X, Y) (((X) < (Y)) ? (X) : (Y))
#define QUEUE_NAME 8321

#define SERVER_INFO "uhttp/0.1.0"

typedef enum {
  GET, POST, UNIMPLEMENTED
} request_t;

void exit_(int);
void parseArgs(int, char**, char**);
void parseConfig(char*, int*, char***, int*, char**, char**, char**);
void parseConfigLine(char*, int*, char***, int*, char**, char**, char**);
void cleanup();

typedef struct {
  int type; // 2 - info, 1 - error
  time_t time;
  char text[512];
} logmsg_t;

int startlogger();
void closequeue();
void savelog(logmsg_t);
void printMessage(logmsg_t);
void log_(int, char*);
int qid;
int loggerpid;
FILE* oklogfile;
FILE* errlogfile;

void startserver();
void shutdownserver();
void* connection_handler(void*);
int get_line(int, char*, int);
request_t parse_request_line(char*, char**);
void parse_host(char*, char**);
void analyze_header(char*, int*);
int send_response(int, char*, char*);
int send_file(int, char*);

char* execName;
int    port;
char** deff;
int    deff_n;
char*  errlog;
char*  oklog;
char*  root;
int    root_len;

int running = true;

int main(int argc, char* argv[]) {
  execName = argv[0];
  char* config_file;
  parseArgs(argc-1, argv+1, &config_file);
  parseConfig(config_file, &port, &deff, &deff_n, &errlog, &oklog, &root);

  signal(SIGINT, shutdownserver);
  sigset_t sigmask;
  sigemptyset(&sigmask);
  sigaddset(&sigmask, SIGPIPE);
  sigprocmask(SIG_SETMASK, &sigmask, NULL);
  loggerpid = startlogger();
  startserver();

  cleanup();
  return 0;
}

void closequeue() {
  if(msgctl(qid, IPC_RMID, 0) < 0)
    perror(NULL);
  fclose(oklogfile);
  fclose(errlogfile);
}

void printMessage(logmsg_t msg) {
  printf("PRINT (%d): %s\n", msg.type, msg.text);
}

void saveLog(logmsg_t msg) {
  FILE* logfile;
  if (msg.type == 2) logfile = oklogfile;
  else logfile = errlogfile;

  char timebuf[40];
  struct tm* timeinfo = localtime(&msg.time);
  strftime(timebuf, 35, "%c", timeinfo);

  fprintf(logfile, "[%s] %s\n", timebuf, msg.text);
}

int startlogger() {
  int pid = fork();
  if(pid != 0){
    while(qid <= 0){
      qid = msgget(QUEUE_NAME, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
      usleep(100);
    }
    return pid;
  }
  qid = msgget(QUEUE_NAME, IPC_CREAT | S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
  atexit(closequeue);

  oklogfile = fopen(oklog, "a");
  errlogfile = fopen(errlog, "a");

  logmsg_t buf;
  while(running)
    if(msgrcv(qid, &buf, sizeof(logmsg_t) - sizeof(long), 0, 0) > 0)
      saveLog(buf);

  exit(0);
}

void log_(int type, char* text) {
  logmsg_t buf;
  buf.type = type;
  time(&buf.time);
  sprintf(buf.text, "%s", text);

  msgsnd(qid, &buf, sizeof(buf) - sizeof(long), 0);
}

void startserver() {
  printf("Starting uHTTP server on port %d\n", port);
  printf("Hosting directory: %s\n", root);
  printf("     error logs -> %s\n", errlog);
  printf("           logs -> %s\n", oklog);
  printf("Files lookup order:\n");
  for (int i=0; i<deff_n; i++)
    printf("  %s\n", deff[i]);

  int socket_desc = socket(AF_INET, SOCK_STREAM, 0);
  int client_sock;
  if (socket_desc < 0) exit_(3);
  struct sockaddr_in server, client;
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = INADDR_ANY;
  server.sin_port = htons(port);
  if (bind(socket_desc, (struct sockaddr*) &server, sizeof(server)) < 0) exit_(3);
  listen(socket_desc, 5);
  printf("\nServer successfully started! Waiting for connections.\n");
  int c = sizeof(struct sockaddr_in);
  int* sock;
  while(running && (client_sock = accept(socket_desc, (struct sockaddr*) &client, (socklen_t*) &c)) >= 0) {
    pthread_t thread;
    sock = malloc(sizeof(client_sock));
    *sock = client_sock;
    if (pthread_create(&thread, NULL, connection_handler, (void*) sock) < 0) exit_(4);
    usleep(1000);
  }
}

int get_line(int sock, char* buf, int size) {
  int read = 0;
  int read_size;
  char c = '\0';
  while (read < size - 1 && c != '\n') {
    read_size = recv(sock, &c, 1, 0);
    if (read_size <= 0) break;
    if (c == '\r')
      recv(sock, &c, 1, 0);
    buf[read++] = c;
  }
  buf[read] = '\0';
  if (buf[read-1] == '\n')
    buf[--read] = '\0';
  if (buf[read-1] == '\r')
    buf[--read] = '\0';
  return read;
}

request_t parse_request_line(char* line, char** path) {
  request_t type = UNIMPLEMENTED;
  int i = 0;
  if (strncmp(line, "GET", 3) == 0) {
    type = GET;
    i = 4;
  }
  else if (strncmp(line, "POST", 4) == 0) {
    type = POST;
    i = 5;
  }

  *path = malloc(sizeof(char) * (strlen(line + i)+ 1));
  int j = 0;
  while (line[i+j] != ' ') {
    (*path)[j] = line[i+j];
    j++;
  }
  (*path)[j] = '\0';
  return type;
}

void parse_host(char* line, char** host) {
  *host = malloc(sizeof(char) * (strlen(line) + 1));
  sscanf(line, "Host: %s", *host);
}

void analyze_header(char* header, int* content_length){
  sscanf(header, "Content-Length: %d", content_length);
}

int http_error(int sock, int error_no) {
  char buf[1024];
  char code_info[32];

  switch (error_no) {
  case 404: strcpy(code_info, "NOT FOUND"); break;
  default: strcpy(code_info, "UNKNOWN ERROR");
  }
  sprintf(buf, "HTTP/1.0 %d %s\r\n", error_no, code_info);
  send(sock, buf, strlen(buf), 0);
  sprintf(buf, "\r\n");
  send(sock, buf, strlen(buf), 0);
  sprintf(buf, "<h1>%d %s</h1>\r\n", error_no, code_info);
  send(sock, buf, strlen(buf), 0);

  return error_no;
}

int send_response(int sock, char* path, char* body) {
  int path_len = strlen(path);
  char* filepath = malloc(sizeof(char) * (root_len + path_len + 1));

  int file_found = false;
  strcpy(filepath, root);
  strcpy(filepath + root_len, "/");
  strcpy(filepath + root_len + 1, path);

  struct stat buf;
  // check if file exists
  if (stat(filepath, &buf) != 0) {
    free(filepath);
    return http_error(sock, 404);
  }

  if (S_ISDIR(buf.st_mode) && filepath[path_len + root_len] == '/') {
    for(int i=0; i<deff_n && !file_found; i++) {
      free(filepath);
      filepath = malloc(sizeof(char) * (root_len + path_len + strlen(deff[i]) + 1));
      strcpy(filepath, root);
      strcpy(filepath + root_len, "/");
      strcpy(filepath + root_len + 1, path);
      strcpy(filepath + root_len + 1 + path_len, "/");
      strcpy(filepath + root_len + 1 + path_len + 1, deff[i]);

      if (stat(filepath, &buf) == 0 && S_ISREG(buf.st_mode)) file_found = true;
    }
  } else if (S_ISREG(buf.st_mode)) file_found = true;

  if (!file_found) {
    free(filepath);
    return http_error(sock, 404);
  }

  int status_code = send_file(sock, filepath);
  free(filepath);
  return status_code;
}

void send_headers(int sock) {
  char buf[1024];
  sprintf(buf, "HTTP/1.0 200 OK\r\n");
  send(sock, buf, strlen(buf), 0);
  sprintf(buf, "Server: %s\r\n", SERVER_INFO);
  send(sock, buf, strlen(buf), 0);
  sprintf(buf, "Content-Type: text/html\r\n");
  send(sock, buf, strlen(buf), 0);
  sprintf(buf, "\r\n");
  send(sock, buf, strlen(buf), 0);
}

int send_file(int sock, char* filepath) {
  send_headers(sock);

  FILE* f = fopen(filepath, "r");
  char buf[1024];
  while(fgets(buf, 1024, f))
    if (buf[0])
      send(sock, buf, strlen(buf), 0);
  fclose(f);
  return 0;
}

void* connection_handler(void* arg) {
  int sock = * ((int*)arg);
  int read_size;
  char buffer[1024];
  request_t type;
  char* path = "";
  char* host = "";
  char* body = "";
  int content_length = 0;
  read_size = get_line(sock, buffer, 1024);
  type = parse_request_line(buffer, &path);
  read_size = get_line(sock, buffer, 1024);
  parse_host(buffer, &host);

  char logmsg[511];
  sprintf(logmsg, "%s %s", (type == GET ? "GET" : type == POST ? "POST" : "UNSUPPORTED"), path);
  log_(2, logmsg);

  while ((read_size = get_line(sock, buffer, 1024)) > 0) {
    // there is header line in buffer
    if (type == POST)
      analyze_header(buffer, &content_length);
  }
  if (type == POST && content_length > 0) {
    body = malloc(sizeof(char) * (content_length + 1));
    read_size = get_line(sock, body, content_length + 1);
  }

  if (read_size < 0) {
    char logmsg[511];
    sprintf(logmsg, "failed to receive request");
    log_(1, logmsg);
  }
  int rcode;
  if ((rcode = send_response(sock, path, body)) != 0) {
    char logmsg[511];
    sprintf(logmsg, "error: %d | %s", rcode, path);
    log_(1, logmsg);
  }
  close(sock); // close connection
  if (type == POST && content_length > 0)
    free(body);
  free(path);
  free(host);
  free(arg);
  return NULL;
}

void shutdownserver() {
  running = false;
  if (loggerpid){
    kill(loggerpid, SIGINT);
    printf("The server is being shut down\n");
  }
  cleanup();
  exit(0);
}

void cleanup() {
  free(oklog);
  free(errlog);
  for (int i=0; i<deff_n; i++)
    free(deff[i]);
  free(deff);
}

void parseArgs(int argc, char* argv[], char** config_file) {
  if (argc != 1) exit_(1);
  *config_file = argv[0];

  struct stat buf;
  // check if file exists
  if (stat(*config_file, &buf) != 0) exit_(2);
}

void parseConfigLine(char* buffer, int* port, char*** deff, int* deff_n, char** errlog, char** oklog, char** root) {
  int i;
  for (i=0; isspace(buffer[i]);) i++;

  if (strncmp("port", buffer, 4) == 0) {
    i += 5;
    for (; isspace(buffer[i]);) i++;
    if (buffer[i] == '=') {i++; for (; isspace(buffer[i]);) i++;}
    buffer += i;

    *port = atoi(buffer);
  }

  if (strncmp("order", buffer, 5) == 0) {
    i += 6;
    for (; isspace(buffer[i]);) i++;
    if (buffer[i] == '=') {i++; while (isspace(buffer[i])) i++;}
    buffer += i;

    int def_i = 0;
    for (i=0; buffer[i] != '\0';) {
      if (!isspace(buffer[i])) def_i++;
      while (!isspace(buffer[i])) i++;
      buffer[i++] = '\0';
    }

    *deff_n = def_i;

    *deff = malloc(sizeof(char*) * (*deff_n));

    int j;
    for (i=0, j=0; i<def_i; i++) {
      (*deff)[i] = malloc(sizeof(char) * (strlen(buffer + j)+1));
      strcpy((*deff)[i], buffer + j);
      j += strlen(buffer + j) + 1;
    }
  }

  if (strncmp("error_log", buffer, 9) == 0) {
    i += 10;
    for (; isspace(buffer[i]);) i++;
    if (buffer[i] == '=') {i++; while (isspace(buffer[i])) i++;}
    buffer += i;

    for (i=0; !isspace(buffer[i]);) i++;
    buffer[i] = '\0';
    *errlog = malloc(sizeof(char) * (strlen(buffer)+1));
    strcpy(*errlog, buffer);
  }

  if (strncmp("ok_log", buffer, 6) == 0) {
    i += 7;
    for (; isspace(buffer[i]);) i++;
    if (buffer[i] == '=') {i++; while (isspace(buffer[i])) i++;}
    buffer += i;

    for (i=0; !isspace(buffer[i]);) i++;
    buffer[i] = '\0';
    *oklog = malloc(sizeof(char) * (strlen(buffer)+1));
    strcpy(*oklog, buffer);
  }

  if (strncmp("root", buffer, 4) == 0) {
    i += 5;
    for (; isspace(buffer[i]);) i++;
    if (buffer[i] == '=') {i++; while (isspace(buffer[i])) i++;}
    buffer += i;

    for (i=0; !isspace(buffer[i]);) i++;
    buffer[i] = '\0';
    *root = malloc(sizeof(char) * (strlen(buffer)+1));
    strcpy(*root, buffer);
  }
}

void parseConfig(char* config_file, int* port, char*** deff, int* deff_n, char** errlog, char** oklog, char** root) {
  FILE* f = fopen(config_file, "r");
  if (f == NULL) exit_(2);
  char buffer[256];

  if (fgets(buffer, 256, f) == NULL || strcmp("[server]\n", buffer) != 0) exit_(2);
  for (int i=0; i<5; i++) {
    if (fgets(buffer, 256, f) == NULL) exit_(2);
    parseConfigLine(buffer, port, deff, deff_n, errlog, oklog, root);
  }

  if (*port <= 0 || *deff_n <= 0
      || root == NULL || strlen(*root) <= 0
      || errlog == NULL || strlen(*errlog) <= 0
      || oklog == NULL || strlen(*oklog) <= 0
      || root == NULL || strlen(*root) <= 0
      ) exit_(2);
  for (int i=0; i< *deff_n; i++) if (strlen((*deff)[i]) <= 0) exit_(2);
  root_len = strlen(*root);
  fclose(f);
}

void exit_(int code) {
  switch (code) {
  case 4:
    printf("Could not create thread\n");
    kill(loggerpid, SIGINT);
    break;
  case 3:
    printf("Could not create socket\n");
    kill(loggerpid, SIGINT);
    break;
  case 2:
    printf("Pass a valid config file\n");
    break;
  default:
    printf("Usage: %s FILE\n", execName);
    printf("  FILE - config file name\n\n");
    printf("FILE should be an .ini file with one section: [server]\n\n");
    printf("Supported parameters:\n");
    printf("  port      - port number to run the server on\n");
    printf("  order     - default file names to host (ordered)\n");
    printf("  error_log - log file for errors\n");
    printf("  ok_log    - standard log file\n");
    printf("\nMaximum line length is 256.\n");
    break;
  }
  cleanup();
  exit(code);
}

from argparse import ArgumentParser
from collections import defaultdict
from configparser import ConfigParser, NoOptionError
import os
from os.path import abspath, isdir, exists, isfile
from socket import socket, SOL_SOCKET, SO_REUSEADDR
from threading import Thread

HOST = ''
PORT = 8080

flag = True


def request_complete(r):
    return r[-4:] == '\r\n\r\n'


def response_string(status, body):
    messages = defaultdict(lambda: "Unknown error")
    messages[200] = "OK"
    messages[400] = "Bad request"
    messages[403] = "Forbidden"
    messages[404] = "Not found"
    messages[500] = "Internal Server Error"
    body = messages[status] if not body else body
    return """HTTP/1.1 {} {}
Content-Type: text/html; charset=UTF-8
Content-Encoding: UTF-8
Content-Length: {}
Server: uhttp by mbiel
Connection: close

{}""".format(status, messages[status], len(body), body).encode('utf-8')


def generate_response(options, path, method):
    requested_path = abspath(options['root'] + path)
    if not exists(requested_path):
        return 404, None
    if isdir(requested_path):
        requested_path = os.path.join(requested_path, options['base_file'])

    if not isfile(requested_path):
        return 403, None

    try:
        with open(requested_path) as f:
            body = f.read()
        return 200, body
    except PermissionError:
        return 403, None
    except Exception as e:
        print("Problem reading file: {}".format(e))
        return 500, None


def parse_request(r, config):
    request = r.split('\r\n')
    method, path, _ = request[0].split()
    options = {key.strip(): value.strip()
               for key, value
               in [line.split(':', maxsplit=1)
                   for line in request[1:]
                   if ':' in line]
               }

    if options['Host'] not in config:
        status = 500
        body = "Internal server error"
    else:
        status, body = generate_response(config[options['Host']], path, method)

    return response_string(status, body)


def process_request(connection, serverconfig):
    with connection:
        request = ""
        while not request_complete(request):
            data = connection.recv(1024)
            request += data.decode('utf-8')
        response = parse_request(request, serverconfig)
        connection.sendall(response)


def runserver(serverconfig):
    with socket() as s:
        s.bind((HOST, PORT))
        s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        s.listen()
        try:
            while True:
                conn, addr = s.accept()
                Thread(target=process_request, args=(conn, serverconfig)).start()
        except KeyboardInterrupt:
            print('Server stopped')


def main():
    parser = ArgumentParser(description='Server configuration.')
    parser.add_argument('config', metavar='config-file', type=str, nargs=1,
                        help='.ini file with the configuration')
    default_section = 'default'
    config_file = parser.parse_args().config
    config = ConfigParser(default_section=default_section)
    config.read(config_file)
    if not config.sections():
        print('Specified file does not contain a configuration')
        exit(1)

    configdict = defaultdict(dict)
    try:
        for section in config.sections() + [default_section]:
            configdict[section]['root'] = config.get(section, 'root')
            configdict[section]['base_file'] = config.get(section, 'base_file')
    except NoOptionError:
        print('Specified configuration is not complete')
        exit(2)

    runserver(configdict)


if __name__ == '__main__':
    main()

#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <unistd.h>
#include "common.h"
#include "common1.h"

#define MAX_CLIENTS 100

void vexit(int, char*, ...);
int registerClient(key_t);
void respondRegister(keymsg_t);
void respondEcho(textmsg_t);
void respondAllCaps(textmsg_t);
void respondTime(textmsg_t);
void respondFinish(textmsg_t);
void close_queue();
void killClient(key_t);

int qid;
key_t clients[MAX_CLIENTS];
int registered_clients;
int to_be_deleted;

int main(int argc, char* argv[]) {
  qid = msgget(QUEUE_NAME, IPC_CREAT | S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
  if (qid == -1) {
    perror(NULL);
    exit(qid);
  }

  atexit(close_queue);

  keymsg_t keymsg_buf;
  textmsg_t textmsg_buf;
  while (1) {
    if (msgrcv(qid, &keymsg_buf, sizeof(keymsg_t), QMSG_REGISTER, IPC_NOWAIT) > 0)
      respondRegister(keymsg_buf);
    else if (msgrcv(qid, &textmsg_buf, sizeof(textmsg_t), QMSG_ECHO, IPC_NOWAIT) > 0)
      respondEcho(textmsg_buf);
    else if (msgrcv(qid, &textmsg_buf, sizeof(textmsg_t), QMSG_ALLCAPS, IPC_NOWAIT) > 0)
      respondAllCaps(textmsg_buf);
    else if (msgrcv(qid, &textmsg_buf, sizeof(textmsg_t), QMSG_TIME, IPC_NOWAIT) > 0)
      respondTime(textmsg_buf);
    else if (msgrcv(qid, &textmsg_buf, sizeof(textmsg_t), QMSG_FINISH, IPC_NOWAIT) > 0)
      respondFinish(textmsg_buf);
    else if (to_be_deleted)
      exit(0);
    usleep(1000);
  }
  return 0;
}

void close_queue() {
  for (int i=0; i<registered_clients; i++)
    killClient(clients[i]);

  if (msgctl(qid, IPC_RMID, 0) < 0) {
    perror(NULL);
    exit(3);
  }
}

void respondFinish(textmsg_t textmsg_buf) {
  to_be_deleted = 1;
}

void killClient(key_t id) {
  textmsg_t msg = {QMSG_FINISH, 0, ""};
  msgsnd(id, &msg, sizeof(msg), 0);
}

void respondTime(textmsg_t textmsg_buf) {
  nowtostr(textmsg_buf.text);
  respondEcho(textmsg_buf);
}

void respondAllCaps(textmsg_t textmsg_buf) {
  strtoupper(textmsg_buf.text);
  respondEcho(textmsg_buf);
}

void respondEcho(textmsg_t textmsg_buf) {
  char* text = textmsg_buf.text;
  key_t childQueue = clients[textmsg_buf.id];
  textmsg_t msg = {QMSG_TEXT, 0, ""};
  strcpy(msg.text, text);
  msgsnd(childQueue, &msg, sizeof(msg), 0);
}

void respondRegister(keymsg_t keymsg_buf) {
  int client_id = registerClient(keymsg_buf.id);
  idmsg_t msg = {QMSG_CONFIRM, client_id};
  msgsnd(keymsg_buf.id, &msg, sizeof(idmsg_t), 0);
}

int registerClient(key_t key) {
  clients[registered_clients] = key;
  return registered_clients++;
}

void vexit(int code, char* message, ...) {
  va_list args;
  va_start(args, message);
  vfprintf(stderr, message, args);
  va_end(args);
  exit(code);
}

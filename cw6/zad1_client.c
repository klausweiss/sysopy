#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <unistd.h>
#include "common1.h"
#include "common.h"

void sendKey(key_t);

int serverQueue;
void newPrompt();

void request_echo(char*);
void request_allcaps(char*);
void request_time();
void request_finish();
void print_help();

void close_queue();

int qid;
int parentIndex;
int waiting_for_response;

int main(int argc, char* argv[]) {
  char* homeDir = getenv("HOME");
  key_t qkey = ftok(homeDir, getpid());
  serverQueue = msgget(QUEUE_NAME, S_IWUSR | S_IRUSR | S_IWGRP | S_IROTH | S_IWOTH);
  if (serverQueue == -1) {
    fprintf(stderr, "Run server first\n");
    exit(2);
  }

  atexit(close_queue);
  signal(SIGINT, request_finish);

  qid = msgget(qkey, IPC_CREAT | S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);

  sendKey(qid);
  waiting_for_response = 1;

  idmsg_t idmsg_buf;
  textmsg_t textmsg_buf;
  while (1) {
    if (msgrcv(qid, &idmsg_buf, sizeof(idmsg_t), QMSG_CONFIRM, IPC_NOWAIT) > 0) {
      parentIndex = idmsg_buf.id;
      printf("SysOpy :: zestaw 6 :: zadanie 1\n");
      waiting_for_response = 0;
    }
    else if (msgrcv(qid, &textmsg_buf, sizeof(textmsg_t), QMSG_TEXT, IPC_NOWAIT) > 0) {
      printf("%s\n", textmsg_buf.text);
      waiting_for_response = 0;
    }
    else if (msgrcv(qid, &textmsg_buf, sizeof(textmsg_t), QMSG_FINISH, IPC_NOWAIT) > 0)
      exit(4);
    else {
      if (idmsg_buf.mtype == 0 || waiting_for_response) continue;
      newPrompt();
      char* input = calloc(128, sizeof(char));
      read(0, input, 128);
      if (strlen(input) == 0) {
        printf("\n");
        exit(0);
      }
      input[strlen(input)-1] = '\0'; // remove trailing \n

      if (beginsWith(input, ECHO_COMMAND)) {
        request_echo(input+5);
        waiting_for_response = 1;
      }
      else if (beginsWith(input, ALL_CAPS_COMMAND)) {
        request_allcaps(input+8);
        waiting_for_response = 1;
      }
      else if (beginsWith(input, TIME_COMMAND)) {
        request_time();
        waiting_for_response = 1;
      }
      else if (beginsWith(input, FINISH_COMMAND)) {
        request_finish();
        waiting_for_response = 1;
      }
      else if (strcmp(input, HELP_COMMAND) == 0)
        print_help();

      free(input);
    }
  }

  return 0;
}

void close_queue() {
  if (msgctl(qid, IPC_RMID, 0) < 0) {
    perror(NULL);
    exit(3);
  }
}

void print_help() {
  printf("Usage:\n\
      echo INPUT     - echoes the input\n\
      allcaps INPUT  - prints the input in all caps\n\
      time           - prints the time\n\
      finish         - finishes the server as soon as its queue is empty\n");
}

void request_echo(char str[MAX_STRLEN]) {
  textmsg_t msg = {QMSG_ECHO, parentIndex, ""};
  strcpy(msg.text, str);
  msgsnd(serverQueue, &msg, sizeof(msg), 0);
}

void request_allcaps(char* str) {
  textmsg_t msg = {QMSG_ALLCAPS, parentIndex, ""};
  strcpy(msg.text, str);
  msgsnd(serverQueue, &msg, sizeof(msg), 0);
}

void request_time() {
  textmsg_t msg = {QMSG_TIME, parentIndex, ""};
  msgsnd(serverQueue, &msg, sizeof(msg), 0);
}

void request_finish() {
  textmsg_t msg = {QMSG_FINISH, parentIndex, ""};
  msgsnd(serverQueue, &msg, sizeof(msg), 0);
  exit(0);
}

void newPrompt() {
  printf("> ");
  fflush(stdout);
}

void sendKey(key_t key) {
  keymsg_t msg = {QMSG_REGISTER, getpid(), key};
  msgsnd(serverQueue, &msg, sizeof(keymsg_t), 0);
}

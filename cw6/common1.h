#ifndef __common_zad1
#define __common_zad1

#include <sys/types.h>

#define MAX_STRLEN 2048
#define QUEUE_NAME 8329

#define QMSG_REGISTER 1
#define QMSG_CONFIRM 2
#define QMSG_TEXT 3
#define QMSG_ECHO 4
#define QMSG_ALLCAPS 5
#define QMSG_TIME 6
#define QMSG_FINISH 21

typedef struct {
  long mtype;
  pid_t pid;
  key_t id;
} keymsg_t;

typedef struct {
  long mtype;
  int id;
} idmsg_t;

typedef struct {
  long mtype;
  int id;
  char text[MAX_STRLEN];
} textmsg_t;

#endif

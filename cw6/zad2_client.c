#include <mqueue.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include "common2.h"
#include "common.h"

void newPrompt();
void parseMessage(posix_msg);
void finishClient(int);

void print_help();
void request_echo(char*);
void request_allcaps(char*);
void request_time();
void request_finish();

int serverid;
mqd_t serverq;
mqd_t qid;
char q_path[10];

int main(int argc, char* argv[]) {
  struct mq_attr attr = {0, 10, sizeof(posix_msg)+1, 0};
  sprintf(q_path, "/%d", getpid());
  serverq = mq_open(QUEUE_NAME, O_WRONLY);
  qid = mq_open(q_path, (O_RDONLY | O_CREAT), S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH, &attr);

  printf("Client started (qid: %d) | server: %d\n", qid, serverq);
  Message msg_body;
  msg_body.pid = getpid();
  posix_msg msg = {QMSG_REGISTER, -1, msg_body};

  int sent_status = mq_send(serverq, (char*) &msg, sizeof(msg_t)+sizeof(int)+sizeof(pid_t), 1);
  if (sent_status < 0 || serverq < 0 || qid < 0) {
    printf("path: %s (sent: %d | server: %d | qid: %d)\n", q_path, sent_status, serverq, qid);
    perror(NULL);
    exit(2);
  }

  int received;
  // register
  received = mq_receive(qid, (char*) &msg, sizeof(posix_msg) + 1, NULL);
  if (received == -1)
    perror("rcv");

  serverid = msg.msg.id;

  while (1) {
    if (msg.type > 0)
      parseMessage(msg);

    msg.type = 0;
    usleep(1000);
    newPrompt();

    char* input = calloc(128, sizeof(char));
    read(0, input, 128);
    if (strlen(input) == 0) {
      printf("\n");
      exit(0);
    }
    input[strlen(input)-1] = '\0'; // remove trailing \n

    if (strcmp(input, HELP_COMMAND) == 0) {
      print_help();
      free(input);
      continue;
    }
    else if (beginsWith(input, ECHO_COMMAND)) {
      request_echo(input+5);
    }
    else if (beginsWith(input, ALL_CAPS_COMMAND)) {
      request_allcaps(input+8);
    }
    else if (beginsWith(input, TIME_COMMAND)) {
      request_time();
    }
    else if (beginsWith(input, FINISH_COMMAND)) {
      request_finish();
    }
    else {
      free(input);
      continue;
    }

    received = mq_receive(qid, (char*) &msg, sizeof(posix_msg) + 1, NULL);
    if (received == -1) {
      perror("rcv");
      continue;
    }

    free(input);
  }

  mq_close(serverq);
  mq_close(qid);
  mq_unlink(q_path);
  return 0;
}

void parseMessage(posix_msg msg) {
  switch (msg.type) {
    case QMSG_ID:
      printf("SysOpy :: zestaw 6 :: zadanie 2\n");
      break;
    case QMSG_ECHO:
      printf("%s\n", msg.msg.text);
      break;
    case QMSG_FINISH:
      finishClient(3);
    default:
      printf("*Message is not handled yet*\n");
      break;
  }
}
void newPrompt() {
  printf("> ");
  fflush(stdout);
}

void print_help() {
  printf("Usage:\n\
      echo INPUT     - echoes the input\n\
      allcaps INPUT  - prints the input in all caps\n\
      time           - prints the time\n\
      finish         - finishes the server as soon as its queue is empty\n");
}

void request_echo(char str[MAX_STRLEN]) {
  Message msg_body;
  strcpy(msg_body.text, str);
  posix_msg msg = {QMSG_ECHO, serverid, msg_body};
  int sent_status = mq_send(serverq, (char*) &msg, strlen(str)+sizeof(int)+sizeof(msg_t), 1);
  if (sent_status < 0)
    exit(3);
}

void request_allcaps(char* str) {
  Message msg_body;
  strcpy(msg_body.text, str);
  posix_msg msg = {QMSG_ALLCAPS, serverid, msg_body};
  int sent_status = mq_send(serverq, (char*) &msg, sizeof(msg), 1);
  if (sent_status < 0)
    exit(3);
}

void request_time() {
  Message msg_body;
  posix_msg msg = {QMSG_TIME, serverid, msg_body};
  int sent_status = mq_send(serverq, (char*) &msg, sizeof(msg), 1);
  if (sent_status < 0)
    exit(3);
}

void request_finish() {
  Message msg_body;
  posix_msg msg = {QMSG_FINISH, serverid, msg_body};
  int sent_status = mq_send(serverq, (char*) &msg, sizeof(msg), 1);
  if (sent_status < 0)
    exit(3);
  exit(0);
}

void finishClient(int status) {
  mq_close(serverq);
  mq_close(qid);
  mq_unlink(q_path);
  exit(status);
}

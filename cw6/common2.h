#ifndef __common_zad1
#define __common_zad1

#define MAX_STRLEN 2048
#define QUEUE_NAME "/tmp"

typedef enum {
  QMSG_REGISTER = 1,
  QMSG_ID,
  QMSG_ECHO,
  QMSG_TIME,
  QMSG_ALLCAPS,
  QMSG_FINISH
} msg_t;

struct register_pair_t {
  pid_t pid;
  char q_path[10];
};

union Message {
  pid_t pid;
  int id;
  char text[MAX_STRLEN];
};

typedef union Message Message;

typedef struct {
  msg_t type;
  int id;
  Message msg;
} posix_msg;

#endif

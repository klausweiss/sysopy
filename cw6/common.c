#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "common.h"

int beginsWith(char* string, char* prefix) {
  int i=0;
  while (string[i] != '\0' && prefix[i] != '\0') {
    if (string[i] != prefix[i])
      return 0;
    i++;
  }
  return (prefix[i] == '\0' ? 1 : 0);
}

void strtoupper(char* buffer) {
  int i=0;
  while (buffer[i] != '\0') {
    buffer[i] = toupper(buffer[i]);
    i++;
  }
}

void nowtostr(char* buffer) {
  time_t rawtime;
  time(&rawtime);
  struct tm* time_info = localtime(&rawtime);
  strftime(buffer, 80, "%x - %I:%M%p", time_info);
}

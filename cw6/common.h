#ifndef __common_header
#define __common_header

#define ECHO_COMMAND "echo "
#define ALL_CAPS_COMMAND "allcaps "
#define TIME_COMMAND "time"
#define FINISH_COMMAND "finish"
#define HELP_COMMAND "help"

int beginsWith(char*, char*);
void strtoupper(char*);
void nowtostr(char*);

#endif

#include <errno.h>
#include <fcntl.h>
#include <mqueue.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include "common2.h"
#include "common.h"

#define MAX_CLIENTS 100

void parseMessage(posix_msg);
void registerClient(Message);
void respondEcho(mqd_t, char*);
void respondAllCaps(mqd_t, char*);
void respondTime(mqd_t);
void respondFinish();

void finishClient(mqd_t);
void finishAllClients();
void killServer();

mqd_t qid;

mqd_t clients[MAX_CLIENTS];
int registered_clients;

int main(int argc, char* argv[]) {
  struct mq_attr attr = {0, 10, sizeof(posix_msg)+1, 0};
  qid = mq_open(QUEUE_NAME, (O_RDWR | O_CREAT), 0644, &attr);

  if (qid == -1) {
    perror("Server queue creation");
    exit(1);
  }

  posix_msg msg;
  ssize_t received;

  while (1) {
    usleep(1000);

    received = mq_receive(qid, (char*) &msg, sizeof(posix_msg)+1, NULL);
    if (received == -1) {
      perror(NULL);
      continue;
    }
    parseMessage(msg);
    memset(&msg, 0, sizeof(posix_msg));
  }

  mq_close(qid);
  mq_unlink(QUEUE_NAME);
  return 0;
}

void parseMessage(posix_msg msg) {
    switch (msg.type) {
      case QMSG_REGISTER:
        registerClient(msg.msg);
        break;
      case QMSG_ECHO:
        respondEcho(clients[msg.id], msg.msg.text);
        memset(&(msg.msg), 0, strlen(msg.msg.text));
        break;
      case QMSG_TIME:
        respondTime(clients[msg.id]);
        break;
      case QMSG_FINISH:
        respondFinish();
        break;
      case QMSG_ALLCAPS:
        respondAllCaps(clients[msg.id], msg.msg.text);
        memset(&(msg.msg), 0, strlen(msg.msg.text));
        break;
      default:
        printf("That's unexpected (%d vs %d | %d)\n", msg.type, QMSG_REGISTER, QMSG_ID);
    }
}

void registerClient(Message msg) {
  char q_path[10];
  sprintf(q_path, "/%d", msg.pid);
  mqd_t client_qid = mq_open(q_path, O_WRONLY);
  clients[registered_clients] = client_qid;

  Message msg_body;
  msg_body.id = registered_clients++;
  posix_msg msg_back = (posix_msg) {QMSG_ID, 0, msg_body};

  int sent_status = mq_send(client_qid, (char*) &msg_back, sizeof(msg_back)+1, 2);
  if (sent_status < 0)
    perror("client send");
}

void respondEcho(mqd_t qid, char* text) {
  Message msg_body;
  strcpy(msg_body.text, text);
  posix_msg msg_back = (posix_msg) {QMSG_ECHO, 0, msg_body};

  int sent_status = mq_send(qid, (char*) &msg_back, sizeof(msg_back)+1, 2);
  if (sent_status < 0)
    perror("client send");
}

void respondAllCaps(mqd_t qid, char* text) {
  strtoupper(text);
  respondEcho(qid, text);
}

void respondTime(mqd_t qid) {
  char now[26];
  nowtostr(now);
  respondEcho(qid, now);
}

void respondFinish() {
  killServer();
}

void finishAllClients() {
  for (int i=registered_clients; i --> 0;)
    finishClient(clients[i]);
}

void finishClient(mqd_t client_qid) {
  Message msg_body;
  posix_msg msg_back = (posix_msg) {QMSG_FINISH, 0, msg_body};

  int sent_status = mq_send(client_qid, (char*) &msg_back, sizeof(msg_t) + sizeof(int) + sizeof(char*), 2);
  if (sent_status < 0)
    perror("client kill");

  mq_close(client_qid);
}

void killServer() {
  finishAllClients();
  mq_close(qid);
  mq_unlink(QUEUE_NAME);
  exit(0);
}

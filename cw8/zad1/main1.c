#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include "record.h"

void parseargs(int, char**, int*, char**, char**);
void exit_(int);
void readRecords(char*, int, FILE*);

struct targs_t {
  char* word;
  int n_records;
};

void runthreads(char*, int, int);
void* findword(void*);
void waitforthreads(int);
void notifyfound(pthread_t, int);

char* execname;
FILE* f;
pthread_mutex_t mutex;
pthread_t* threads;
int n_threads;
int allread = 1;

int main(int argc, char* argv[]) {
  int n_records;
  char *filename, *word;

  execname = argv[0];
  parseargs(argc-1, argv+1, &n_records, &filename, &word);
  threads = (pthread_t*) calloc(n_threads, sizeof(pthread_t));

  f = fopen(filename, "r");
  if (f == NULL) exit_(5);
  allread = 0;
  pthread_mutex_init(&mutex, NULL);

  runthreads(word, n_threads, n_records);
  waitforthreads(n_threads);

  pthread_mutex_destroy(&mutex);
  fclose(f);
  free(threads);
  return 0;
}

void waitforthreads(int N) {
  for (int i=0; i<N; i++)
    if(pthread_join(threads[i], NULL))
      exit_(6);
}

void notifyfound(pthread_t tid, int id) {
  printf("Found: %d (%ld)\n", id, (long) tid);
  for (int i=0; i<n_threads; i++)
    if (threads[i] && !pthread_equal(threads[i], tid)) {
      printf(" closing %ld\n", (long) threads[i]);
      pthread_cancel(threads[i]);
    }
  printf(" finished closing\n");
}

void waiting_thread_cleanup(void *arg) {
  pthread_mutex_unlock((pthread_mutex_t*) arg);
}

void* findword(void* arg) {
  pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);

  struct targs_t* args = (struct targs_t*) arg;
  int max_length = args->n_records * sizeof(record_t);
  record_t* records = malloc(max_length);

  int found = 0;
  while (!allread && !found) {
    pthread_mutex_lock(&mutex);
    pthread_cleanup_push(waiting_thread_cleanup, (void*) &mutex);
    readRecords((char*) records, args->n_records, f);
    pthread_testcancel();
    pthread_mutex_unlock(&mutex);
    pthread_cleanup_pop(1);

    for (int i=0; i<args->n_records; i++)
      if (strlen(records[i].text) == 0) break;
      else if (strstr(records[i].text, args->word) != NULL) {
        notifyfound(pthread_self(), records[i].id);
        found = 1;
        break;
      }
  }
  free(records);
  return NULL;
}

void runthreads(char* w, int n_threads, int n_records) {
  struct targs_t args = {w, n_records};
  pthread_mutex_lock(&mutex);
  for (int i=0; i<n_threads; i++)
    pthread_create(&threads[i], NULL, findword, (void*) &args);
  pthread_mutex_unlock(&mutex);
}

void readRecords(char* records, int N, FILE* f) {
  int length = N * sizeof(record_t);
  records[0] = getc(f);
  if (records[0] == EOF) {
    allread = 1;
    return;
  }

  for (int i=1; i<length; i++)
    records[i] = getc(f);
}

void parseargs(int argc, char* argv[], int* n_records, char* filename[], char* word[]) {
  if (argc != 4) exit_(1);
  n_threads = atoi(argv[0]);
  *n_records = atoi(argv[2]);
  *filename = argv[1];
  *word = argv[3];

  struct stat buf;
  // check if file exists
  if (stat(*filename, &buf) != 0) exit_(4);
  if (n_threads <= 0) exit_(2);
  if (*n_records <= 0) exit_(3);
}

void exit_(int code) {
  switch (code) {
  case 1:
    printf("Usage: %s THREADS FILE RECORDS WORD\n", execname);
    printf("  THREADS  - number of threads to run\n");
    printf("  FILE     - file with records\n");
    printf("  RECORDS  - number of records to read at once\n");
    printf("  WORD     - word to look for\n");
    break;
  case 2:
    printf("number of threads must be grater than 0\n");
    break;
  case 3:
    printf("number of records must be grater than 0\n");
    break;
  case 4:
    printf("Specified file does not exist\n");
    break;
  case 5:
    printf("Failed to open the file\n");
    break;
  case 6:
    printf("Filed joining thread\n");
    break;
  }
  exit(code);
}

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "record.h"

void parseargs(int, char**, int*);
void exit_(int);
void randomstring(char*, int);
void generateRecords(int);

char* execname;

int main(int argc, char* argv[]) {
  srand(time(NULL));
  int N;
  execname = argv[0];
  parseargs(argc-1, argv+1, &N);

  generateRecords(N);
  return 0;
}

void generateRecords(int N) {
  record_t r;
  r.id = 0;
  for (int i=0; i<N; i++) {
    randomstring(r.text, sizeof(r.text));
    r.id++;
    writeRecord(r);
  }
}

void randomstring(char* str, int size) {
  for (int i=0; i<size; i++)
    str[i] = "abcdefghijklmnopqrstuvwxyz  "[random() % 28];
}

void parseargs(int argc, char* argv[], int* N) {
  if (argc != 1) exit_(1);
  *N = atoi(argv[0]);
  if (*N <= 0) exit_(2);
}

void exit_(int code) {
  switch (code) {
  case 1:
    printf("Usage: %s N\n", execname);
    printf("  N  - number of records to generate\n");
    break;
  case 2:
    printf("N must be grater than 0\n");
  }
  exit(code);
}

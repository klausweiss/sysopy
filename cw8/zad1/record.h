#ifndef __record
#define __record

#include <stdio.h>

typedef struct {
  int id;
  char text[1020];
} record_t;

void writeRecord(record_t);

void writeRecord(record_t r) {
  int length = sizeof(r);
  fwrite(&r, length, 1, stdout);
}

#endif

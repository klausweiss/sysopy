#include <netinet/ip.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#define MAX_CLIENTS 10

typedef enum {computation, ping, taken} rtype;
typedef struct {
  rtype type;
  char op;
  int x;
  int y;
  int request_n;
} request;
typedef struct {
  rtype type;
  double data;
  int request_n;
} response;

void parseArgs(int, char**);
void exit_(int);
void destroy();

void startClient();
void registerClient();
void unregister();
void allowConnections();
double compute(char, int, int);
void sendData(double, int);
void resendPing();

char* execname;
int port;
int id;
struct sockaddr_in sa;
int socketFD;
int res;

int main(int argc, char* argv[]) {
  execname = argv[0];
  parseArgs(argc-1, argv+1);
  signal(SIGINT, unregister);
  atexit(destroy);

  startClient();
  registerClient();
  allowConnections();

  return 0;
}

void allowConnections() {
  request buf;
  int res;
  double val;
  while ((res = read(socketFD, &buf, sizeof buf)) > 0) {
    switch (buf.type) {
    case computation:
      val = compute(buf.op, buf.x, buf.y);
      sendData(val, buf.request_n);
      printf("%d %c %d = %f\n", buf.x, buf.op, buf.y, val);
      break;
    case taken:
      exit_(6);
      break;
    case ping:
      /* resendPing(); */
      break;
    }
  }
}

void resendPing() {
  response res = {ping, 0, 0};
  write(socketFD, &res, sizeof res);
}

void sendData(double data, int request_n) {
  response res = {computation, data, request_n};
  usleep(500000);
  write(socketFD, &res, sizeof res);
}

double compute(char op, int x, int y) {
  switch(op) {
  case '+':
    return x + y;
  case '-':
    return x - y;
  case '*':
    return x * y;
  case '/':
    if(y){
      return 1.0 * x / y;
    }
  default:
    return 0;
  }
}

void registerClient() {
  char buffer[10];
  sprintf(buffer, "%d", id);
  write(socketFD, buffer, 10);
}

void startClient() {
  socketFD = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (socketFD == -1) exit_(4);

  memset(&sa, 0, sizeof sa);
  sa.sin_family = AF_INET;
  sa.sin_port = htons(port);
  res = inet_pton(AF_INET, "127.0.0.1", &sa.sin_addr);

  if (connect(socketFD, (struct sockaddr*) &sa, sizeof sa) < 0) {
    close(socketFD);
    exit_(4);
  }
}

void parseArgs(int argc, char* argv[]) {
  if (argc != 2) exit_(1);
  port = atoi(argv[0]);
  id = atoi(argv[1]);
  if (port < 1000 || port > 65000) exit_(2);
  if (id < 0 || id >= MAX_CLIENTS) exit_(3);
}

void exit_(int code) {
  switch(code) {
  case 1:
    printf("Usage: %s port id\n", execname);
    printf("    port  - port to the server was run on\n");
    printf("    id    - client name\n");
    break;
  case 2:
    printf("port is not valid (should be 1000 < port < 65000)\n");
    break;
  case 3:
    printf("id is not valid (should be 0 < port < %d)\n", MAX_CLIENTS);
    break;
  case 4:
    printf("Could not create socket\n");
    break;
  case 5:
    printf("Client unregistered. Closing\n");
    exit(0);
    break;
  case 6:
    printf("Client with that name is already registered\n");
    break;
  }
  exit(code);
}

void destroy() {
  shutdown(socketFD, SHUT_RDWR);
  close(socketFD);
}

void unregister() {
  destroy();
  exit_(5);
}

#include <netinet/ip.h>
#include <pthread.h>
#include <signal.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>

#define MAX_CLIENTS 10

typedef enum {computation, ping, taken} rtype;
typedef struct {
  rtype type;
  char op;
  int x;
  int y;
  int request_n;
} request;
typedef struct {
  rtype type;
  double data;
  int request_n;
} response;

void parseArgs(int, char**);
void exit_(int);
void exitGracefully();
void destroy();

void printPrompt();
void startServer();
void* initServer(void*);
void startSupervisor();
void* initSupervisor(void*);
void startRepl();
void notifyTaken();
int computeRemotely(char, int, int);
void* waitForClient(void*);

int running;
char* execname;
int port;
int* clients;
int* busy;
pthread_t* threads;
int connected_n;
int socket_desc;
int request_n = 1;

int main(int argc, char* argv[]) {
  srand(time(NULL));
  execname = argv[0];
  parseArgs(argc-1, argv+1);
  atexit(destroy);
  signal(SIGINT, exitGracefully);

  startServer();
  startSupervisor();
  startRepl();

  return 0;
}

void startRepl() {
  while (!running) usleep(1000);
  printf("Usage: [+/-*] X Y\n");
  char op;
  int x, y;
  char* buf = malloc(256 * sizeof(char));
  size_t len;
  while (1) {
    printPrompt();
    getline(&buf, &len, stdin);
    sscanf(buf, "%c %d %d", &op, &x, &y);
    if (op == 32 || op == 9) continue;
    if (computeRemotely(op, x, y) == -1) printf("`0` is invalid divisor\n");
  }
  free(buf);
  printf("\n");
}

void printPrompt() {
  printf("(%d)> ", request_n);
  fflush(stdout);
}

void* waitForClient(void* args) {
  int clientid = *((int*) args);
  response buf;
  read(clients[clientid], &buf, sizeof buf);
  printf("(cli: %d | req: %d) = %f\n", clientid, buf.request_n, buf.data);
  printPrompt();
  fflush(stdout);
  busy[clientid] = 0;
  free(args);
  return NULL;
}

int computeRemotely(char op, int x, int y) {
  if (!connected_n) {
    printf("There are no clients connected\n");
    return -1;
  }
  if (op == '/' && y == 0) return -1;
  int clientid = random() % MAX_CLIENTS;
  int* cliid;
  while (connected_n && (clients[clientid] == -1 || busy[clientid])) clientid = (clientid + random()) % MAX_CLIENTS;
  request buf = {computation, op, x, y, request_n};
  busy[clientid] = 1;
  write(clients[clientid], &buf, sizeof buf);
  request_n++;
  cliid = malloc(sizeof(int));
  *cliid = clientid;
  if (pthread_create(&threads[clientid], NULL, waitForClient, (void*) cliid) < 0) exit_(3);
  return 0;
}

void* initSupervisor(void* _args) {
  while (1) {
    for (int i=0; i<MAX_CLIENTS; i++) {
      usleep(250000);

      response buf;
      recv(clients[i], &buf, sizeof buf, MSG_DONTWAIT | MSG_PEEK);
      if (clients[i] != -1 && errno == 9) {
        close(clients[i]);
        clients[i] = -1;
        busy[i] = 0;
        connected_n -= 1;
      }
    }
  }
  return NULL;
}

void startSupervisor() {
  pthread_t supervisor_thread;
  if (pthread_create(&supervisor_thread, NULL, initSupervisor, (void*) NULL) < 0) exit_(3);
}

void startServer() {
  clients = malloc(sizeof(int) * MAX_CLIENTS);
  busy = malloc(sizeof(int) * MAX_CLIENTS);
  threads = malloc(sizeof(int) * MAX_CLIENTS);
  for (int i=0; i<MAX_CLIENTS; i++){
    clients[i] = threads[i] = -1;
    busy[i] = 0;
  }
  pthread_t thread;
  if (pthread_create(&thread, NULL, initServer, (void*) NULL) < 0) exit_(3);
}

void* initServer(void* args) {
  socket_desc = socket(AF_INET, SOCK_STREAM, 0);
  if (socket_desc < 0) exit_(2);
  struct sockaddr_in server, client;
  int client_sock;
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = INADDR_ANY;
  server.sin_port = htons(port);
  if (bind(socket_desc, (struct sockaddr*) &server, sizeof(server)) < 0) exit_(2);
  listen(socket_desc, 5);
  printf("Waiting for connections on port %d.\n", port);
  running = 1;

  int c = sizeof(struct sockaddr_in);
  char buffer[1024];
  int i;
  while ((client_sock = accept(socket_desc, (struct sockaddr*) &client, (socklen_t*) &c)) >= 0) {
    read(client_sock, buffer, 10);
    i = atoi(buffer);
    if(clients[i] == -1){
      clients[i] = client_sock;
      connected_n++;
      /* printf("Client (%d) connected: %d\n", i, client_sock); */
    }
    else notifyTaken(client_sock);
  }

  return NULL;
}

void parseArgs(int argc, char* argv[]) {
  if (argc != 1) exit_(1);
  port = atoi(argv[0]);
  if (port < 1000 || port > 65000) exit_(2);
}

void exit_(int code) {
  switch(code) {
  case 0:
    printf("Server has been shut down\n");
    break;
  case 1:
    printf("Usage: %s port\n", execname);
    printf("    port  - port to run the server on\n");
    break;
  case 2:
    printf("port is not valid (should be 1000 < port < 65000)\n");
    break;
  }
  exit(code);
}

void notifyTaken(int sock) {
  request buf;
  buf.type = taken;
  write(sock, &buf, sizeof buf);
}

void destroy() {
  free(clients);
  free(busy);
  free(threads);
}

void exitGracefully() {
  exit_(0);
}

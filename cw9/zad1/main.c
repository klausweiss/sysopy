#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

#define true 1
#define false 0
#define not !

#define MAX_VAL 100


void parseArgs(int, char**);
void exit_(int);

void start_writers();
void start_readers();

void* writer_action(void*);
void* reader_action(void*);
void find_divisible(int);
void modify_array();

char* execname;
int READERS;
int WRITERS;
int M;
int N;
int verbose;

int* data;
sem_t readsem;
sem_t writesem;
int readers = 0;

pthread_t* threads;


int main(int argc, char* argv[]) {
  srand(time(NULL));

  execname = argv[0];
  parseArgs(argc-1, argv+1);
  data = malloc(M * sizeof(int));
  for (int i=0; i<M; i++) data[i] = 1 + rand() % MAX_VAL;
  threads = malloc((READERS + WRITERS) * sizeof(pthread_t));
  if (sem_init(&readsem, 0, 1) < 0) exit_(2);
  if (sem_init(&writesem, 0, 1) < 0) exit_(2);

  start_writers();
  start_readers();

  for (int i=0; i<WRITERS+READERS; i++)
    pthread_join(threads[i], NULL);
  free(data);
  free(threads);
  return 0;
}

int random_time() {return 1000 + (rand() % 250000);}

void* writer_action(void* arg) {
  for (int i=0; i<N; i++) {
    sem_wait(&writesem);
    modify_array();
    sem_post(&writesem);
    usleep(random_time());
  }
  return NULL;
}

void find_divisible(int divisor) {
  int count = 0;
  long tid = pthread_self();
  for (int i=0; i<M; i++) {
    if (data[i] % divisor == 0) {
      count++;
      if (verbose) printf("\033[1;32m(%d, %d)\033[m ", i, data[i]);
    }
  }
  if (verbose && count > 0) printf("\n");
  printf("Znaleziono \033[1;32m%d\033[m liczb podzielnych przez \033[1;32m%d\033[m\n", count, divisor);
}

void modify_array() {
  int count = rand() % M;
  long tid = pthread_self();
  int index, newval;
  for (int i=0; i<count; i++) {
    index = rand() % M;
    newval = rand() % MAX_VAL;
    data[index] = newval;
    if (verbose) printf("\033[1;33m(%d: %d)\033[m ", index, newval);
  }
  if (verbose && count > 0) printf("\n");
  printf("Zmodyfikowano \033[1;33m%d\033[m liczb\n", count);
}

void* reader_action(void* arg) {
  int* divisor = (int*) arg;
  for (int i=0; i<N; i++) {
    sem_wait(&readsem);
    readers += 1;
    if (readers == 1)
      sem_wait(&writesem);
    sem_post(&readsem);
    find_divisible(*divisor);
    sem_wait(&readsem);
    readers -= 1;
    if (readers == 0)
      sem_post(&writesem);
    sem_post(&readsem);
    usleep(random_time());
  }
  free(divisor);
  return NULL;
}

void start_writers() {
  for (int i=0; i<WRITERS; i++){
    if (pthread_create(&threads[i], NULL, writer_action, NULL) < 0) exit_(1);
  }
}

void start_readers() {
  for (int i=0; i<READERS; i++){
    int* divisor = malloc(sizeof(int));
    *divisor = 2 + random() % (MAX_VAL - 1);
    if (pthread_create(&threads[WRITERS + i], NULL, reader_action, (void*) divisor) < 0) exit_(1);
  }
}

void parseArgs(int argc, char* argv[]) {
  if (argc < 4 || argc > 5) exit_(3);
  READERS = atoi(argv[0]);
  WRITERS = atoi(argv[1]);
  M = atoi(argv[2]);
  N = atoi(argv[3]);
  if (READERS <= 0 || WRITERS <= 0 || N <= 0 || M <= 0) exit_(3);
  if (argc > 4 && strcmp(argv[4], "-l") == 0) verbose = true;
}

void exit_(int code) {
  switch(code) {
  case 1:
    printf("Could not start a thread\n");
    break;
  case 2:
    printf("Could not init semaphore\n");
    break;
  case 3:
    printf("Usage: %s READERS WRITERS M N [-l]\n", execname);
    printf("    READERS - number of readers (>0)\n");
    printf("    WRITERS - number of readers (>0)\n");
    printf("    M       - array size (>0)\n");
    printf("    N       - number of read/write times (>0)\n");
    printf("    -l      - verbose output\n");
    break;
  }
  exit(code);
}

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>


void parseArgumentsOrExit(int, char* [], char**);
void processBatchFile(FILE*);
int printHelp();
void executeProgram(char*);
int  countArgs(char*);

char* execName;

int main(int argc, char* argv[]) {
  execName = argv[0];
  char* fileName = NULL;
  parseArgumentsOrExit(argc - 1, argv + 1, &fileName);

  FILE* file = fopen(fileName, "r");
  processBatchFile(file);
  fclose(file);

  return 0;
}


void parseArgumentsOrExit(int argc, char *argv[], char** file) {
    /*
     * Negative error codes do not print the help.
     * Positive error codes do.
     */

    /*
     * number of argument:
     *  1
     */
    if (argc != 1)
        exit(printHelp());
    
    /*
     * input/output file
     */

    struct stat buf;
    if (stat(argv[0], &buf) != 0) {
        printf("Specified file (%s) does not exist.\n", argv[0]);
        exit(-1);
    }

    *file = argv[0];
}

int printHelp() {
  printf("  Usage: %s INPUT_FILE\n", execName);
  return 10;
}


void processBatchFile(FILE* file) {
  char* line = NULL;
  char* envVar;
  char* newValue;
  size_t lineLength;

  while(getline(&line, &lineLength, file) != -1) { // this is being executed twice
    if (line[0] == '#') {
      envVar = strtok(line, " \n") + 1;
      newValue = strtok(NULL, "\n");
      if (newValue == NULL)
        unsetenv(envVar);
      else
        setenv(envVar, newValue, 1); // 1 -- overwrite
    } else {
      executeProgram(line);
    }
  }
  free(line);
}

void executeProgram(char* line) {
  char* program;
  char* arguments;
  int result;
  int argc;

  program = strtok(line, " \n");
  arguments = line + 1 + strlen(program);
  argc = countArgs(arguments);

  int status;
  pid_t pid = fork();

  char* args[argc+2];
  char* ch;
  // parse arguments string to arguments array

  args[0] = "";
  int i = 0;
  ch = strtok(arguments, " \n\0");
  while (ch != NULL) {
    args[++i] = ch;
    ch = strtok(NULL, " \n\0");
  }
  args[i+1] = NULL;

  // endparse arguments string to arguments array
  
  if (pid == -1)
    printf("Could not fork a process for `%s`\n", program);
  else if (pid > 0) {
    waitpid(pid, &status, 0);

    if (status != 0)
      exit(status);
  }
  else {
    if (arguments[0] != '\0') // if any argument was passed
      result = execvp(program, args);
    else
      result = execlp(program, "", NULL);

    if (result == -1) {
      printf("\033[0;31m------------- %s (%s) ------------\033[0m\n", strerror(errno), program);
      _exit(EXIT_FAILURE);
    }
  }
}

int countArgs(char* arguments) {
  char* ptr = arguments;
  int count = 0;
  while(*ptr != '\0') {
    if(*ptr == ' ' || *ptr == '\n')
      count++;
    ++ptr;
  }
  return count;
}

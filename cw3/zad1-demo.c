#include <stdio.h>
#include <stdlib.h>


int main(int argc, char* argv[]) {
  char* value = NULL;
  for (int i=1; i<argc; i++) {
    value = getenv(argv[i]);
    if(value != NULL)
      printf("\033[1;32m%s = %s\033[0m\n", argv[i], value);
    else
      printf("\033[1;31m%s is not set\033[0m\n", argv[i]);
  }
  return 0;
}

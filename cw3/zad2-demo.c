int dig(int);

int main(int argc, char* argv[]) {
  int tooDeep = dig(1);
  return tooDeep;
}

int dig(int depth) {
  while (depth) {
    depth += 1;
  }
  dig(1);

  return depth;
}

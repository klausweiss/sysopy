#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>


void parseArgumentsOrExit(int, char* [], char**, int*, int*);
void processBatchFile(FILE*, int, int);
int printHelp();
void executeProgram(char*, int, int);
int  countArgs(char*);
void setlimits(int, int);
void printUsage(char*);

char* execName;

int main(int argc, char* argv[]) {
  execName = argv[0];
  char* fileName = NULL;
  int maxTime;
  int maxVStorage;
  parseArgumentsOrExit(argc - 1, argv + 1, &fileName, &maxTime, &maxVStorage);

  FILE* file = fopen(fileName, "r");
  processBatchFile(file, maxTime, maxVStorage);
  fclose(file);

  return 0;
}


void parseArgumentsOrExit(int argc, char *argv[], char** file, int* maxTime, int* maxVStorage) {
    /*
     * Negative error codes do not print the help.
     * Positive error codes do.
     */

    /*
     * number of argument:
     *  3
     */
    if (argc != 3)
        exit(printHelp());
    
    /*
     * input/output file
     */

    struct stat buf;
    if (stat(argv[0], &buf) != 0) {
        printf("Specified file (%s) does not exist.\n", argv[0]);
        exit(-1);
    }

    *file = argv[0];

    /*
     * max CPU time
     */
    *maxTime = atoi(argv[1]);
    if (*maxTime <= 0) {
        printf("Cannot execute programs in 0 or less seconds (%ds).\n", *maxTime);
        exit(-2);
    }

    /*
     * max virtual storage
     */
    *maxVStorage = atoi(argv[2]);
    if (*maxVStorage <= 0) {
        printf("Cannot use less than or 0 megabytes (%dMB).\n", *maxVStorage);
        exit(-3);
    }
}

int printHelp() {
  printf("  Usage: %s INPUT_FILE MAX_CPU_TIME MAX_VIRTUAL_STORAGE\n", execName);
  return 10;
}


void processBatchFile(FILE* file, int maxTime, int maxVStorage) {
  char* line = NULL;
  char* envVar;
  char* newValue;
  size_t lineLength;

  while(getline(&line, &lineLength, file) != -1) { // this is being executed twice
    if (line[0] == '#') {
      envVar = strtok(line, " \n") + 1;
      newValue = strtok(NULL, "\n");
      if (newValue == NULL)
        unsetenv(envVar);
      else
        setenv(envVar, newValue, 1); // 1 -- overwrite
    } else {
      executeProgram(line, maxTime, maxVStorage);
    }
  }
  free(line);
}

void executeProgram(char* line, int maxTime, int maxVStorage) {
  char* program;
  char* arguments;
  int result;
  int argc;

  program = strtok(line, " \n");
  arguments = line + 1 + strlen(program);
  argc = countArgs(arguments);

  int status;
  pid_t pid = fork();

  char* args[argc+2];
  char* ch;
  // parse arguments string to arguments array

  args[0] = "";
  int i = 0;
  ch = strtok(arguments, " \n\0");
  while (ch != NULL) {
    args[++i] = ch;
    ch = strtok(NULL, " \n\0");
  }
  args[i+1] = NULL;

  // endparse arguments string to arguments array
  
  if (pid == -1)
    printf("Could not fork a process for `%s`\n", program);
  else if (pid > 0) {
    waitpid(pid, &status, 0);
    printUsage(program);

    if (status != 0)
      exit(status);
  }
  else {
    setlimits(maxTime, maxVStorage);
    if (arguments[0] != '\0') // if any argument was passed
      result = execvp(program, args);
    else
      result = execlp(program, "", NULL);

    if (result == -1) {
      printf("\033[0;31m------------- %s (%s) ------------\033[0m\n", strerror(errno), program);
      _exit(EXIT_FAILURE);
    }
  }
}

int countArgs(char* arguments) {
  char* ptr = arguments;
  int count = 0;
  while(*ptr != '\0') {
    if(*ptr == ' ' || *ptr == '\n')
      count++;
    ++ptr;
  }
  return count;
}

void setlimits(int maxTime, int maxVStorage) {
  struct rlimit timeLimit = {maxTime, maxTime}; 
  struct rlimit storageLimit = {maxVStorage * 1024 * 1024, maxVStorage * 1024 * 1024}; 

  setrlimit(RLIMIT_CPU, &timeLimit);
  setrlimit(RLIMIT_AS, &storageLimit);
}

double timevalToDouble(struct timeval tval) {
  return (tval.tv_sec * 1000000 + tval.tv_usec) / 1000000.0;
}

void printUsage(char* program) {
  struct rusage usage;
  getrusage(RUSAGE_CHILDREN, &usage);
  double stime = timevalToDouble(usage.ru_stime);
  double utime = timevalToDouble(usage.ru_utime);
  printf("\033[1;36m");
  printf("\t| CPU USAGE (\033[1;37m%s\033[1;36m):\n", program);
  printf("\t|  user:   %.4f\n", utime);
  printf("\t|  system: %.4f", stime);
  printf("\033[0;0;0;0;0m\n");
}

#include <fcntl.h>
#include <semaphore.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>

#include "common.h"
#include "fifo.h"

int printHelp();
void parseArgumentsOrExit(int, char* [], int*);
void shutdownGracefully();
char* execName;

void initsem();
void rmsem();
void initshm();
void rmshm();

void fallAsleep();

int N;
sem_t *semid, *actionsemid;
int shmid, Nid;
fifo_t* fifoaddr;
int* Naddr;
sem_t** sems;

int main(int argc, char* argv[]) {
  execName = argv[0];
  parseArgumentsOrExit(argc - 1, argv + 1, &N);
  atexit(rmsem);
  signal(SIGINT, shutdownGracefully);

  initsem();
  initshm();

  pid_t pid;
  int index;
  while (1) {
    fallAsleep();
    index = fifoaddr->first;
    pid = fifoPop(fifoaddr);
    if (pid == -1) continue;
    printf("Cutting %d\n", pid);
    sem_post(sems[index]);
    printf("%d cut\n", pid);
  }

  return 0;
}

int printHelp() {
  printf("  Usage: %s N\n", execName);
  return 10;
}

void parseArgumentsOrExit(int argc, char *argv[], int* N) {
    if (argc != 1)
        exit(printHelp());

    *N = atoi(argv[0]);

    if (*N <= 0) {
      printf("N cannot be 0 or less\n");
      exit(2);
    }
}

void initshm() {
  Nid = shm_open(NNAME, (O_RDWR | O_CREAT), 0644);
  ftruncate(Nid, sizeof(int));
  Naddr = mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED, Nid, 0);
  *Naddr = N;

  size_t size = sizeof(fifo_t) + N*sizeof(pid_t);
  shmid = shm_open(SHMNAME, (O_RDWR | O_CREAT), 0644);
  ftruncate(shmid, size);
  fifoaddr = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, shmid, 0);
  *fifoaddr = *newFifo(N);
}

void rmshm() {
  munmap(Naddr, sizeof(int));
  shm_unlink(NNAME);
  munmap(fifoaddr, sizeof(fifo_t) + N*sizeof(pid_t));
  shm_unlink(SHMNAME);
}

void initsem() {
  actionsemid = sem_open(ACTIONSEM, (O_RDWR | O_CREAT), 0644, 1);
  semid = sem_open(SEMNAME, (O_RDWR | O_CREAT), 0644, 0);
  sems = malloc(N * sizeof(sem_t*));

  char name[10];
  for (int i=0; i<N; i++) {
    sprintf(name, "/%d", i);
    sems[i] = sem_open(name, (O_RDWR | O_CREAT), 0644, 0);
  }
}

void rmsem() {
  sem_close(semid);
  sem_unlink(SEMNAME);
  sem_close(actionsemid);
  sem_unlink(ACTIONSEM);

  char name[10];
  for (int i=0; i<N; i++) {
    sprintf(name, "/%d", i);
    sem_close(sems[i]);
    sem_unlink(name);
  }
  free(sems);
}

void shutdownGracefully() {
  exit(0);
}

void fallAsleep() {
  int value;
  sem_getvalue(semid, &value);
  if (value == 0)
    printf(" [...] Falling asleep...\n");
  sem_wait(semid);
}

#include <fcntl.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <time.h>
#include <unistd.h>

#include "fifo.h"
#include "common.h"

#define gettime() clock_gettime(CLOCK_MONOTONIC, &tp)

int printHelp();
void parseArgumentsOrExit(int, char* [], int*, int*);
char* execName;

int C, S, N;
int timescut;

void initsem();
void initshm();

int N;
sem_t *semid, *actionsemid;
int shmid;
fifo_t* fifoaddr;
sem_t** sems;

struct timespec tp;

void clientAction();

int main(int argc, char* argv[]) {
  execName = argv[0];
  parseArgumentsOrExit(argc - 1, argv + 1, &C, &S);

  initshm();
  initsem();

  for (int i=0; i<C; i++) {
    if (fork() == 0) {
      while (timescut < S)
        clientAction();
      return 0;
    }
  }
  return 0;
}

int printHelp() {
  printf("  Usage: %s C S\n", execName);
  return 10;
}

void parseArgumentsOrExit(int argc, char *argv[], int* C, int* S) {
    if (argc != 2)
        exit(printHelp());

    *C = atoi(argv[0]);

    if (*C <= 0) {
      printf("C cannot be 0 or less\n");
      exit(2);
    }

    *S = atoi(argv[1]);

    if (*S <= 0) {
      printf("S cannot be 0 or less\n");
      exit(2);
    }
}

void clientAction() {
  timescut++;
  sem_wait(actionsemid);
  gettime();
  if (!fifoFull(fifoaddr)) {
    if (!fifoEmpty(fifoaddr))
      printf("%d [%d:%8d]    I'm taking a seat in the queue\n", getpid(), (int) tp.tv_sec, (int) tp.tv_nsec);
    else
      printf("%d [%d:%8d]   I'm sitting directly on the chair\n", getpid(), (int) tp.tv_sec, (int) tp.tv_nsec);
    int index = fifoPush(fifoaddr, getpid());
    printf("%d [%d:%8d]      Waiting to get a haircut\n", getpid(), (int) tp.tv_sec, (int) tp.tv_nsec);
    sem_post(actionsemid);
    sem_post(semid);
    sem_wait(sems[index]);
    gettime();
    printf("%d [%d:%8d] cut!\n", getpid(), (int) tp.tv_sec, (int) tp.tv_nsec);
  } else {
    printf("%d [%d:%8d]          There are no free spots => I'm leaving\n", getpid(), (int) tp.tv_sec, (int) tp.tv_nsec);
    sem_post(actionsemid);
  }
}

void initshm() {
  int Nid = shm_open(NNAME, (O_RDWR), 0644);
  ftruncate(Nid, sizeof(int));
  int* Naddr = mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED, Nid, 0);
  N = *Naddr;

  size_t size = sizeof(fifo_t) + N*sizeof(pid_t);
  shmid = shm_open(SHMNAME, (O_RDWR), 0644);
  ftruncate(shmid, size);
  fifoaddr = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, shmid, 0);
}

void initsem() {
  actionsemid = sem_open(ACTIONSEM, (O_RDWR), 0644, 0);
  semid = sem_open(SEMNAME, (O_RDWR), 0644, 0);
  sems = malloc(N * sizeof(sem_t*));

  char name[10];
  for (int i=0; i<N; i++) {
    sprintf(name, "/%d", i);
    sems[i] = sem_open(name, (O_RDWR), 0644, 0);
  }
}

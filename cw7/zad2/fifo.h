#ifndef _fifo
#define _fifo
#include <stdlib.h>

typedef struct {
  int size;
  int elems;
  int first;
  pid_t items[];
} fifo_t;

fifo_t* newFifo(int);
int fifoPush(fifo_t*, pid_t);
int fifoFull(fifo_t*);
int fifoEmpty(fifo_t*);
pid_t fifoPop(fifo_t*);

void printfifoinfo(fifo_t*);

#endif

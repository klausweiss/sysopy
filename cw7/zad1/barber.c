#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <time.h>
#include "common.h"
#include "fifo.h"

#define gettime() clock_gettime(CLOCK_MONOTONIC, &tp)

int printHelp();
void parseArgumentsOrExit(int, char* [], int*);
char* execName;

void initsem();
void initshm(int);
void rmsem();
void rmshm();

void fallAsleep();
void freeCuttingChair();
void closeSalon();
void greacefulshutdown();
void waitForClientToSeat();

int N;
int semid;
int fifoid;
int Nid;
fifo_t* fifoaddr;
pid_t client_pid;

struct timespec tp;

int main(int argc, char* argv[]) {
  execName = argv[0];
  parseArgumentsOrExit(argc - 1, argv + 1, &N);

  initsem();
  initshm(N);
  atexit(closeSalon);
  signal(SIGINT, greacefulshutdown);

  while (1) {
    if (fifoEmpty(fifoaddr))
      fallAsleep();
    waitForClientToSeat();
    int semindex = fifoaddr->first;
    client_pid = fifoPop(fifoaddr);
    if (client_pid == -1) {
      freeCuttingChair();
      continue;
    }
    gettime();
    printf("[%d:%8d] I'm starting cutting %d.\n", (int) tp.tv_sec, (int) tp.tv_nsec, client_pid);
    // cut the client
    semctl(semid, 2 + semindex, SETVAL, 0);
    gettime();
    printf("[%d:%8d] I've finished cutting %d.\n", (int) tp.tv_sec, (int) tp.tv_nsec, client_pid);
    freeCuttingChair();
  }

  return 0;
}

int printHelp() {
  printf("  Usage: %s N\n", execName);
  return 10;
}

void parseArgumentsOrExit(int argc, char *argv[], int* N) {
    if (argc != 1)
        exit(printHelp());

    /*
     * N
     */
    *N = atoi(argv[0]);

    if (*N <= 0) {
      printf("N cannot be 0 or less\n");
      exit(2);
    }
}

void initshm(int N) {
  key_t Nkey = ftok(getenv("HOME"), 'N');
  Nid = shmget(Nkey, sizeof(int), (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH) | IPC_CREAT | IPC_EXCL);
  int* shmN = shmat(Nid, NULL, 0);
  *shmN = N;

  key_t key = ftok(getenv("HOME"), 'f');
  fifoid = shmget(key, sizeof(fifo_t) + N*sizeof(pid_t), (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH) | IPC_CREAT | IPC_EXCL);
  fifoaddr = shmat(fifoid, NULL, 0);

  *fifoaddr = *newFifo(N);
}

void initsem() {
  key_t key = ftok(getenv("HOME"), 'b');
  semid = semget(key, 2 + N, (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH) | IPC_CREAT | IPC_EXCL);
  
  semctl(semid, BAR, SETVAL, 1);
  semctl(semid, CLI, SETVAL, 0);
  for (int i=0; i<N; i++)
    semctl(semid, N + i, SETVAL, 1);
}

void rmsem() {
  semctl(semid, 0, IPC_RMID, 0); 
}

void rmshm() {
  shmctl(fifoid, IPC_RMID, 0); 
  shmctl(Nid, IPC_RMID, 0); 
}

void fallAsleep() {
  if (client_pid != -1)
    printf(" [...] I'm falling asleep... [...]\n");
  struct sembuf sops = {BAR, 0, 0};
  semop(semid, &sops, 1);
}

void freeCuttingChair() {
  semctl(semid, BAR, SETVAL, 1);
  semctl(semid, CLI, SETVAL, 1);
}

void closeSalon() {
  rmsem();
  rmshm();
}

void greacefulshutdown() {
  exit(0);
}

void waitForClientToSeat() {
  struct sembuf sops = {CLI, 0, 0};
  semop(semid, &sops, 1);
}

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include "common.h"
#include "fifo.h"

#define gettime() clock_gettime(CLOCK_MONOTONIC, &tp)

int printHelp();
void parseArgumentsOrExit(int, char* [], int*, int*);
char* execName;

void initshm();
void initsem(int);
void clientAction();

void wakeBarberUp();
void leaveSalon();
void takeSeat();
void waitForHaircut();

int C, S, N;
int semid;
fifo_t* fifoaddr;

int timescut;
int chairid;

struct timespec tp;

int main(int argc, char* argv[]) {
  execName = argv[0];
  parseArgumentsOrExit(argc - 1, argv + 1, &C, &S);

  initshm();
  initsem(N);

  for (int i=0; i<C; i++) {
    if (fork() == 0) {
      while (timescut < S)
        clientAction();
      return 0;
    }
  }

  return 0;
}

int printHelp() {
  printf("  Usage: %s C S\n\tC - number of clients\n\tS - number of cuts\n", execName);
  return 10;
}

void parseArgumentsOrExit(int argc, char *argv[], int* C, int* S) {
    if (argc != 2)
        exit(printHelp());

    /*
     * C
     */
    *C = atoi(argv[0]);

    if (*C <= 0) {
      printf("C cannot be 0 or less\n");
      exit(2);
    }

    /*
     * S
     */
    *S = atoi(argv[1]);

    if (*S <= 0) {
      printf("S cannot be 0 or less\n");
      exit(2);
    }
}

void clientAction() {
  gettime();
  if (fifoEmpty(fifoaddr) && isbarberfree(semid)) {
    wakeBarberUp();
    takeSeat();
    waitForHaircut();
  } else if (isbarberfree(semid)) {
    struct sembuf sops = {BAR, -1, 0};
    semop(semid, &sops, 1);
  } else if (fifoFull(fifoaddr)) {
    leaveSalon();
  } else {
    takeSeat();
    waitForHaircut();
  }
}

void takeSeat() {
  chairid = fifoPush(fifoaddr, getpid());
  if (chairid != fifoaddr->first)
    printf("%d [%d:%8d]    I've took a seat in queue.\n", getpid(), (int) tp.tv_sec, (int) tp.tv_nsec);
  else
    printf("%d [%d:%8d]    I've took a seat directly on a chair.\n", getpid(), (int) tp.tv_sec, (int) tp.tv_nsec);
}

void leaveSalon() {
  printf("%d [%d:%8d]    There were no empty chairs so I left.\n", getpid(), (int) tp.tv_sec, (int) tp.tv_nsec);
}

void wakeBarberUp() {
  struct sembuf sops = {BAR, -1, IPC_NOWAIT};
  semop(semid, &sops, 1);

  if (errno == EAGAIN)
    printf("%d [%d:%8d]  I'm waking the ba... he's been already woken up...\n", getpid(), (int) tp.tv_sec, (int) tp.tv_nsec);
  else
    printf("%d [%d:%8d]  I'm waking the barber up.\n", getpid(), (int) tp.tv_sec, (int) tp.tv_nsec);
}

void waitForHaircut() {
  printf("%d [%d:%8d]      I'm waiting to get my cut done.\n", getpid(), (int) tp.tv_sec, (int) tp.tv_nsec);
  // sit on a chair
  struct sembuf sops = {CLI, -1, 0};
  semop(semid, &sops, 1);
  // wait on `chairid`-th semaphore
  sops = (struct sembuf) {2 + chairid, 0, 0};
  semop(semid, &sops, 1);
  gettime();
  timescut++;
  // set chair for the `not getting a haircut yet`-one
  semctl(semid, 2 + chairid, SETVAL, 1);
  printf("%d [%d:%8d] I've just got a cut.\n", getpid(), (int) tp.tv_sec, (int) tp.tv_nsec);
}

void initsem(int N) {
  key_t key = ftok(getenv("HOME"), 'b');
  semid = semget(key, 2 + N, (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH));
}

void initshm() {
  key_t Nkey = ftok(getenv("HOME"), 'N');
  int Nid = shmget(Nkey, sizeof(int), (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH));  
  if (Nid == -1)
    exit(2);
  int* shmN = shmat(Nid, NULL, 0);
  N = *shmN;

  key_t key = ftok(getenv("HOME"), 'f');
  int fifoid = shmget(key, sizeof(fifo_t) + N*sizeof(pid_t), (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH));
  if (fifoid == -1)
    exit(2);
  fifoaddr = shmat(fifoid, NULL, 0);
}

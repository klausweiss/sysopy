#include <stdlib.h>
#include <stdio.h>
#include "fifo.h"

fifo_t* newFifo(int size) {
  fifo_t* fqueue = malloc(sizeof(fifo_t) + size*sizeof(pid_t));
  fqueue->size = size;
  fqueue->elems = 0;
  fqueue->first = 0;
  return fqueue;
}

int fifoFull(fifo_t* fqueue) {
  return fqueue->elems == fqueue->size;
}

int fifoEmpty(fifo_t* fqueue) {
  return fqueue->elems == 0;
}

int fifoPush(fifo_t* fqueue, pid_t pid) {
  if (fifoFull(fqueue)) return -1;
  int index = (fqueue->first + fqueue->elems) % fqueue->size;
  fqueue->items[index] = pid;
  fqueue->elems++;
  return index;
}

pid_t fifoPop(fifo_t* fqueue) {
  if (fifoEmpty(fqueue)) return -1;
  pid_t elem = fqueue->items[fqueue->first];
  fqueue->first = (fqueue->first + 1) % fqueue->size;
  fqueue->elems--;
  return elem;
}

void printfifoinfo(fifo_t* fq) {
  printf("size:  %d\n", fq->size);
  printf("elems: %d\n", fq->elems);
  printf("first: %d\n", fq->first);
  printf("empty: %d\n", fifoEmpty(fq));
  printf("full:  %d\n", fifoFull(fq));
}

#ifndef common_zad1
#define common_zad1

// whether the barber is free or not
#define BAR 0
// whether there is a free chair or not
#define CLI 1

int getsemval(int, int);
int isbarberfree(int);

int getsemval(int semid, int semn) {
  return semctl(semid, semn, GETVAL, 0); 
}

int isbarberfree(int semid) {
  return getsemval(semid, BAR) != 0;
}

#endif

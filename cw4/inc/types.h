#ifndef _addressbook_types
#define _addressbook_types

typedef enum { PREV, NEXT } Direction;

Direction toggleDirection(Direction);

#endif

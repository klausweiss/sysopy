#include "types.h"

Direction toggleDirection(Direction dir) {
  if (dir == NEXT)
    return PREV;
  return NEXT;
}

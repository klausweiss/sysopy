#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#define READ 0
#define WRITE 1

int printHelp();
void parseArgumentsOrExit(int, char* [], int*);
void spawnChild(int);
void doChildAction();
void doParentAction(int, int);
void pong(int, siginfo_t*, void*);
void countSignal(int, siginfo_t*, void*);
void finish(int);

char* execName;

int sentSignals;
int receivedSignals;
int returnedSignals;
int childPipe[2];

int sig1;
int sig2;
int type;

int main (int argc, char *argv[]) {
  execName = argv[0];
  
  int L;

  parseArgumentsOrExit(argc - 1, argv + 1, &L);

  pipe(childPipe);
  spawnChild(L);

  char buf[32];
  
  if (type != 2) {
    read(childPipe[READ], &buf, 32);
    receivedSignals = atoi(buf);
  }

  printf("sent: %d | received: %d | returned: %d\n", sentSignals, receivedSignals, returnedSignals);
}

void pong(int signo, siginfo_t* info, void* _) {
  receivedSignals++;
  if (type != 2)
    kill(info->si_pid, sig1);
  else {
    union sigval val = {receivedSignals};
    sigqueue(info->si_pid, sig1, val);
  }
}

void finish(int signo) {
  if (type != 2) {
    char buf[32];
    sprintf(buf, "%d", receivedSignals);
    write(childPipe[WRITE], buf, 32);
    close(childPipe[WRITE]);
  }

  _exit(receivedSignals);
}

void doChildAction() {
  for (;;) {}
}

void countSignal(int signo, siginfo_t* info, void* _) {
  if (type == 2)
    receivedSignals = info->si_value.sival_int;

  returnedSignals++;
}

void doParentAction(int pid, int L) {
  usleep(0);
  if (type != 2) {
    for (int i=0; i<L; i++) {
      kill(pid, sig1);
      sentSignals++;
    }
    kill(pid, sig2);
  } else {
    union sigval val = {0};
    for (int i=0; i<L; i++) {
      sigqueue(pid, sig1, val);
      sentSignals++;
    }
      sigqueue(pid, sig2, val);
  }

  wait(NULL);
}

void spawnChild(int L) {
  pid_t pid = fork();

  // send signal back 
  struct sigaction requestAction;
  requestAction.sa_sigaction = pong;
  sigemptyset(&requestAction.sa_mask);
  requestAction.sa_flags = SA_SIGINFO;

  // receive returned signal
  struct sigaction handleReturn;
  handleReturn.sa_sigaction = countSignal;
  sigemptyset(&handleReturn.sa_mask);
  handleReturn.sa_flags = SA_SIGINFO;

  if (pid < 0)
    printf("Could not fork the process\n");
  else if (pid == 0) {
    sigaction(sig1, &requestAction, NULL);
    signal(sig2, finish);
    close(childPipe[READ]);
    doChildAction();
  } else {
    sigaction(sig1, &handleReturn, NULL);
    close(childPipe[WRITE]);
    doParentAction(pid, L);
  }
}
 
int printHelp() {
  printf("  Usage: %s L Type\n", execName);
  return 10;
}

void parseArgumentsOrExit(int argc, char *argv[], int* L) {
    /*
     * Negative error codes do not print the help.
     * Positive error codes do.
     */

    /*
     * number of argument:
     *  2
     */
    if (argc != 2)
        exit(printHelp());

    /*
     * L
     */
    *L = atoi(argv[0]);

    if (*L <= 0) {
      printf("L cannot be 0 or less\n");
      exit(2);
    }

    /*
     * Type
     */
    type = atoi(argv[1]);

    if (type < 0 || type > 3) {
      printf("Type must be in {1, 2, 3}\n");
      exit(3);
    }

    sig1 = SIGUSR1;
    sig2 = SIGUSR2;

    if (type == 3) {
      sig1 = SIGRTMIN + 3;
      sig2 = SIGRTMIN + 30;
    }
}

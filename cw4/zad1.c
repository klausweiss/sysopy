#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include "inc/types.h"


char getNextLetter(char, Direction);

void changeDirection();
void finishProgram();

Direction dir = NEXT;

int main(int argc, char* argv[]) {
  char actualLetter = 'A';

  signal(SIGTSTP, changeDirection);

  struct sigaction action;
  action.sa_handler = finishProgram;
  sigemptyset(&action.sa_mask);
  action.sa_flags = 0;
  sigaction(SIGINT, &action, NULL);

  for (;;) {
    printf("%c", actualLetter);
    actualLetter = getNextLetter(actualLetter, dir);
    fflush(stdout) ;
    usleep(100000);
  }
  return 0;
}

char getNextLetter(char letter, Direction dir) {
  return 'A' + (letter - 'A' + (dir == NEXT ? 1 : -1) + ('Z' - 'A' + 1)) % ('Z' - 'A' + 1);
}

void changeDirection() {
  printf("\n");
  dir = toggleDirection(dir);
}

void finishProgram() {
  printf("\nOdebrano sygnał SIGINT\n");
  exit(0);
}

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

int printHelp();
void parseArgumentsOrExit(int, char* [], int*, int*);
void spawnChildren(int);
void doChildAction(int);
void acceptRequest(int, siginfo_t*, void*);
void handleDeath(int, siginfo_t*, void*);
void returnToParent(int, siginfo_t*, void*);

char* execName;
int k;
int K;
clock_t requestTime;
pid_t* pids;

int main (int argc, char *argv[]) {
  execName = argv[0];
  srand(time(NULL));

  int N;
  parseArgumentsOrExit(argc - 1, argv + 1, &N, &K);

  pids = (pid_t*) malloc(K * sizeof(pid_t));

  struct sigaction requestAction;
  requestAction.sa_sigaction = acceptRequest;
  sigemptyset(&requestAction.sa_mask);
  requestAction.sa_flags = SA_SIGINFO;
  sigaction(SIGUSR1, &requestAction, NULL);

  struct sigaction continueAction;
  continueAction.sa_sigaction = handleDeath;
  sigemptyset(&continueAction.sa_mask);
  continueAction.sa_flags = SA_SIGINFO;
  for (int i=0; i<32; i++)
    sigaction(SIGRTMIN + i, &continueAction, NULL);

  spawnChildren(N);

  while (k < K) {}
  for (int i=0; i<K; i++)
    kill(pids[i], SIGUSR2);
  while (k < N) {}
}
 
void handleDeath(int signo, siginfo_t* info, void* _) {
  int status = -1;
  int deadId = -10;
  while (deadId != info->si_pid) {
    deadId = wait(&status);
    if (deadId < 0) return;
    if (WIFEXITED(status))
      status = WEXITSTATUS(status);
    printf("  finished: %6d (PID)  %4ds  %3d (signal)\n", deadId, status, signo);
  }
}

void acceptRequest(int signo, siginfo_t* info, void* _) {
  ++k;
  if (k <= K)
    pids[k-1] = info->si_pid;
  else
    kill(info->si_pid, SIGUSR2);
} 

void returnToParent(int signo, siginfo_t* info, void* _) {
  int sendSigno = SIGRTMIN + rand() % 32;
  kill(info->si_pid, sendSigno);
  clock_t finishTime;
  time(&finishTime);
  _exit(finishTime - requestTime);
}

void spawnChildren(int N) {
  pid_t ppid = getpid();
  pid_t pid;
  int sleepFor;

  struct sigaction childFinishAction;
  childFinishAction.sa_sigaction = returnToParent;
  sigemptyset(&childFinishAction.sa_mask);
  childFinishAction.sa_flags = SA_SIGINFO;

  for (int i=0; i<N; i++) {
    usleep(1000); // to give processes time to spawn
    pid = fork();
    
    if (pid < 0) {
      printf("Could not fork the process\n");
      K--;
      N--;
    }
    else if (pid == 0) { // child
      doChildAction(ppid);
      sigaction(SIGUSR2, &childFinishAction, NULL);
      pause();
    }
  }
}

void doChildAction(int ppid) {
  srand(getpid());
  sleepFor = random() % 10;
  sleep(sleepFor);
  time(&requestTime);
  kill(ppid, SIGUSR1);
}

int printHelp() {
  printf("  Usage: %s N K\n", execName);
  return 10;
}

void parseArgumentsOrExit(int argc, char *argv[], int* N, int* K) {
    /*
     * Negative error codes do not print the help.
     * Positive error codes do.
     */

    /*
     * number of argument:
     *  2
     */
    if (argc != 2)
        exit(printHelp());

    /*
     * N
     */
    *N = atoi(argv[0]);

    if (*N <= 0) {
      printf("N cannot be 0 or less\n");
      exit(2);
    }

    /*
     * K
     */
    *K = atoi(argv[1]);

    if (*K <= 0) {
      printf("K cannot be 0 or less\n");
      exit(3);
    }
}

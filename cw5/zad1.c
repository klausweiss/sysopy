#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define PIPE "|"
#define READ 0
#define WRITE 1
#define exec(ARG) execvp(ARG[0], ARG)
#define MAXARGS 3
#define MAXCOMMANDS 20
#define BOLD "\033[1m"
#define GREEN "\033[01;32m"
#define NORMAL "\033[00m"

void parseArgs(int, char**, char**);
void processBatchFile(FILE*);
void processLine(char*, int);

char* execName;

int main(int argc, char* argv[]) {
  
  parseArgs(argc, argv, &execName);

  processBatchFile(stdin);

  return 0;
}

char** stringToArgs(char* str) {
  char** args = (char**) malloc(MAXARGS * sizeof(char*));
  int i = 0;

  while(str[0] == ' ') str++;
  str[strlen(str)-1] = '\0';

  char* pch = strtok(str, " \n\0");
  while (pch != NULL) {
    args[i++] = pch;
    pch = strtok(NULL, " ");
  }
  args[i] = NULL;
  return args;
}

void processLine(char* input, int length) {
  char* inputCpy = (char*) malloc(length * sizeof(char));
  strcpy(inputCpy, input);
  inputCpy[strlen(inputCpy) - 1] = '\0';

  char* pch;
  pch = strtok(input, PIPE);

  int** pipes = (int**) malloc(MAXCOMMANDS * sizeof(int*));
  pipes[0] = (int*) malloc(2 * sizeof(int));
  int pipe_i = 1;
  pipe(pipes[0]);

  if (fork() == 0) {
    char** args = stringToArgs(pch);
    close(pipes[0][READ]);
    dup2(pipes[0][WRITE], STDOUT_FILENO);
    exec(args);
    _exit(0);
  }

  int status;
  wait(&status);
  close(pipes[0][WRITE]);

  // tu wszystko
  pid_t pid;
  pch = strtok(NULL, PIPE);
  while (pch != NULL) {
    pipes[pipe_i] = (int*) malloc(2 * sizeof(int));
    pipe(pipes[pipe_i]);

    pid = fork();
    if (pid == 0) { // child
      char** args = stringToArgs(pch);
      close(pipes[pipe_i][READ]);
      dup2(pipes[pipe_i][WRITE], STDOUT_FILENO);
      dup2(pipes[pipe_i-1][READ], STDIN_FILENO);
      exec(args); 
    } else { // parent
      wait(NULL);
      close(pipes[pipe_i][WRITE]);
      close(pipes[pipe_i-1][READ]);
    }

    pipe_i++;
    pch = strtok(NULL, PIPE);
  }
  
  char* buff = calloc(512, sizeof(char));
  int n;
  printf("    %s%s{ %s }%s\n", BOLD, GREEN, inputCpy, NORMAL);
  while ((n = read(pipes[pipe_i-1][READ], buff, 512)) > 0) {
    printf("%s", buff);
  }
  close(pipes[pipe_i-1][READ]);

  for(int i=0; i<pipe_i; i++) free(pipes[i]);
  printf("\n");
}

void processBatchFile(FILE* file) {
  char* line = NULL;
  size_t lineLength;
  while (getline(&line, &lineLength, file) != -1)
    processLine(line, lineLength);
}

void exitUsage(int code) {
  printf("  Usage: %s FILENAME\n", execName);
  exit(code);
}

void parseArgs(int argc, char** argv, char** execName) {
  *execName = argv[0];
  if (argc != 1) exitUsage(1);
}

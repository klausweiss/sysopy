#ifndef __complex_types
#define __complex_types

typedef struct {
  double x;
  double y;
  int k;
} MandelbrotPoint;

#endif

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include "types.h"

#define READ 0
#define WRITE 1
#define DATA_FILE "data"

void parseArgs(int, char**, char**, int*, char**);
void addPoint(int**, int, MandelbrotPoint);
void saveMandelbrot(int**, int, char*);
void showMandelbrot(int, char*);

char* execName;
char* pipeFile;
int R;

int main(int argc, char* argv[]) {
  
  parseArgs(argc - 1, argv, &execName, &R, &pipeFile);

  int fifo;
  int pipeCreationStatus = mkfifo(pipeFile,0666);
  fifo = open(pipeFile, O_RDONLY);
  if (pipeCreationStatus != 0 && fifo < 0) { // file exists and is not a valid pipe
    printf("\033[0;31m------------- %s (%d) ------------\033[0m\n", strerror(errno), errno);
    exit(5);
  }

  int** T = (int**) malloc(R * sizeof(int*));
  for (int i=0; i<R; i++) T[i] = (int*) calloc(R, sizeof(int));

  MandelbrotPoint buf;
  int bytesread;
  while (1) {
    if ((bytesread = read(fifo, &buf, sizeof(MandelbrotPoint))) > 0) {
      addPoint(T, R, buf);
    } else
      break;
  }

  close(fifo);

  saveMandelbrot(T, R, DATA_FILE);
  showMandelbrot(R, DATA_FILE);

  return 0;
}

void showMandelbrot(int R, char* filename) {
  FILE* fd = popen("gnuplot", "w");
  fprintf(fd, "set view map\n");
  fprintf(fd, "set xrange [0:%d]\n", R);
  fprintf(fd, "set yrange [0:%d]\n", R);
  fprintf(fd, "plot '%s' with image\n", filename);

  fflush(fd);
  getc(stdin);
  pclose(fd);
}

void saveMandelbrot(int** T, int R, char* filename) {
  FILE* f = fopen(filename, "w");
  for (int i=0; i<R; i++)
    for (int j=0; j<R; j++)
      fprintf(f, "%d %d %d\n", i, j, T[i][j]);
  fclose(f);
}

void addPoint(int** T, int w, MandelbrotPoint point) {
  int x, y;
  x = (point.x + 2) / 3 * w;
  y = (point.y + 1) / 2 * w;
  T[x][y] = point.k;
}

void exitUsage(int code) {
  printf("  Usage: %s R PIPEFILE\n", execName);
  exit(code);
}

void parseArgs(int argc, char** argv, char** execName, int* R, char** pipeFile) {
  *execName = argv[0];
  if (argc != 2) exitUsage(1);

  *R = atoi(argv[1]);

  if (*R <= 0) {
    printf("R must not be 0 or less.\n");
    exitUsage(3);
  }

  *pipeFile = argv[2];

  struct stat buf;
  int pipeExists = (stat(*pipeFile, &buf) == 0);
  if (pipeExists && !S_ISFIFO(buf.st_mode)) {
    printf("Specified file (%s) is not a valid pipe (%s).\n", *pipeFile, strerror(errno));
    exitUsage(4);
  }
}

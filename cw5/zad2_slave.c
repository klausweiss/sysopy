#include <complex.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include "types.h"

#define READ 0
#define WRITE 1

void parseArgs(int, char**, char**, int*, int*, char**);
double randomdouble(double, double);
int countIters(double, double);
void saveDataToPipe(int, double, double, int);

char* execName;
char* pipeFile;
int N;
int K;

int main(int argc, char* argv[]) {

  srand(time(NULL));
  
  parseArgs(argc - 1, argv, &execName, &N, &K, &pipeFile);

  int fifo;
  int pipeCreationStatus = mkfifo(pipeFile,0666);
  fifo = open(pipeFile, O_WRONLY);
  if (pipeCreationStatus != 0 && fifo < 0) { // file exists and is not a valid pipe
    printf("\033[0;31m------------- %s (%d) ------------\033[0m\n", strerror(errno), errno);
    exit(5);
  }
  
  double x; 
  double y;
  int iters;

  for (int i=0; i<N; i++) {
    x = randomdouble(-2, 1);
    y = randomdouble(-1, 1);

    iters = countIters(x, y);

    saveDataToPipe(fifo, x, y, iters);
  }

  close(fifo);

  return 0;
}

void saveDataToPipe(int fifo, double x, double y, int iters) {
  MandelbrotPoint point = {x, y, iters};
  write(fifo, &point, sizeof(point));
}

int countIters(double x, double y) {
  double complex c = x + I * y;
  double complex z = 0;
  int k = 0;
  while (cabs(z) <= 2 && k < K) {
    z = cpow(z, 2) + c;
    k++;
  }
  return k;
}

double randomdouble(double min, double max) {
  double divisor = RAND_MAX / (max - min);
  return min + (rand() / divisor);
}

void exitUsage(int code) {
  printf("  Usage: %s N K PIPEFILE\n", execName);
  exit(code);
}

void parseArgs(int argc, char** argv, char** execName, int* N, int* K, char** pipeFile) {
  *execName = argv[0];
  if (argc != 3) exitUsage(1);

  *N = atoi(argv[1]);

  if (*N <= 0) {
    printf("N must not be 0 or less.\n");
    exitUsage(2);
  }

  *K = atoi(argv[2]);

  if (*K <= 0) {
    printf("K must not be 0 or less.\n");
    exitUsage(3);
  }

  *pipeFile = argv[3];

  struct stat buf;
  int pipeExists = (stat(*pipeFile, &buf) == 0);
  if (pipeExists && !S_ISFIFO(buf.st_mode)) {
    printf("Specified file (%s) is not a valid pipe (%s).\n", *pipeFile, strerror(errno));
    exitUsage(4);
  }
}

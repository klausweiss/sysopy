#!/bin/bash

EXEC=$1

function hr {
    printf '%80s\n' | tr ' ' -
}

function generate_cw2_1 {
    $EXEC generate "$1_$2.txt" $1 $2
    cp "$1_$2.txt" "$1_$2_cpy.txt"
}

function run_cw2_1_filename_method {
    $EXEC $1 $2 $5 $3 $4
}

function run_cw2_1 {
    run_cw2_1_filename_method lib $1 $2 $3 "$2_$3.txt"
    run_cw2_1_filename_method sys $1 $2 $3 "$2_$3_cpy.txt"
}

for FILE_SIZE in 512 1024; do
    for RECORD_SIZE in 4 512 4096 8192; do
        generate_cw2_1 $FILE_SIZE $RECORD_SIZE
    done
done
hr

for OPERATION in 'shuffle' 'sort'; do
    for FILE_SIZE in 512 1024; do
        for RECORD_SIZE in 4 512 4096 8192; do
            run_cw2_1 $OPERATION $FILE_SIZE $RECORD_SIZE
            echo "       ----------"
        done
    done
    hr
done

rm [51]*.txt

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include "inc/types.h"
#include "inc/generate.h"
#include "inc/shuffle.h"
#include "inc/sort.h"

int printHelp();
void parseArgumentsOrExit(int, char* [], FunctionsLibrary*, Operation*, FILE**, int*, int*, int*);
void resetTimer();
void measureTime(char*);
char* operationToString(Operation, FunctionsLibrary);

struct rusage startRusage;
struct rusage endRusage;
struct timespec startReal;
struct timespec endReal;

char* execName;

int main (int argc, char *argv[]) {
    execName = argv[0];

    FunctionsLibrary f_library;
    Operation operation;
    FILE* libFile = NULL;
    int sysFile;
    int fileSize;
    int recordsSize;
    parseArgumentsOrExit(argc - 1, argv + 1, &f_library, &operation, &libFile, &sysFile, &fileSize, &recordsSize);

    resetTimer();
    switch (operation) {
        case GENERATE:
            generateRecordFileOrExit(libFile, fileSize, recordsSize);
            break;
        case SHUFFLE:
            srand(time(NULL));
            if (f_library == SYS)
                shuffleRecordsSys(sysFile, fileSize, recordsSize);
            else if (f_library == LIB)
                shuffleRecordsLib(libFile, fileSize, recordsSize);
            break;
        case SORT:
            if (f_library == SYS)
                sortRecordsSys(sysFile, fileSize, recordsSize);
            else if (f_library == LIB)
                sortRecordsLib(libFile, fileSize, recordsSize);
            break;
        default:
            return printHelp();
    }
    printf("%4d %4d", fileSize, recordsSize);
    measureTime(operationToString(operation, f_library));

    if (libFile != NULL)
      fclose(libFile);
    if (sysFile)
      close(sysFile);

    return 0;
}

int printHelp() {
  printf("  Usage: %s sys/lib shuffle/sort input_file file_length record_size\n", execName);
  printf("          or\n");
  printf("         %s generate input_file file_length record_size\n", execName);
  return 10;
}

void parseArgumentsOrExit(int argc, char *argv[], FunctionsLibrary *f_library, Operation* operation, FILE** libFile, int* sysFile, int* fileSize, int* recordsSize) {
    /*
     * Negative error codes do not print the help.
     * Positive error codes do.
     */

    /*
     * number of argument:
     *  5 or 4
     */
    if (argc != 5 && argc != 4) 
        exit(printHelp());
    
    if (argc == 4)
        argv = argv - 1;
    else {
        /*
         * first argument:
         *  lib/sys
         */
        if (strcmp(argv[0], "lib") == 0)
            *f_library = LIB;
        else if (strcmp(argv[0], "sys") == 0)
            *f_library = SYS;
        else {
            printHelp();
            exit(1);
        }
    }

    /*
     * second argument:
     *  generate/shuffle/sort
     */
    if (strcmp(argv[1], "generate") == 0 && argc == 4)
        *operation = GENERATE;
    else if (strcmp(argv[1], "shuffle") == 0 && argc == 5)
        *operation = SHUFFLE;
    else if (strcmp(argv[1], "sort") == 0 && argc == 5)
        *operation = SORT;
    else {
        printHelp();
        exit(2);
    }

    /*
     * input/output file
     */

    struct stat buf;
    if (stat(argv[2], &buf) != 0 && (*operation == SHUFFLE || *operation == SORT)) {
        printf("Specified file (%s) does not exist.\n", argv[2]);
        exit(-1);
    }

    if (*f_library == LIB || *operation == GENERATE) {
      *libFile = fopen(argv[2], "rw+");
      if (*libFile == NULL)
          *libFile = fopen(argv[2], "w+");
    } else {
      *sysFile = open(argv[2], O_RDWR, buf.st_mode);
    }

    /*
     * File size (number of lines)
     */
    *fileSize = atoi(argv[3]);

    if (*fileSize <= 0) {
        printf("Pass a valid positive number (%s).\n", argv[3]);
        exit(-2);
    }

    /*
     * Record size (length of lines)
     */
    *recordsSize = atoi(argv[4]);

    if (*recordsSize <= 0) {
        printf("Pass a valid positive number (%s).\n", argv[4]);
        exit(-3);
    }
}

void resetTimer() {
    getrusage(RUSAGE_SELF, &startRusage);
    clock_gettime(CLOCK_MONOTONIC_RAW, &startReal);
}

void measureTime(char* description) {
    getrusage(RUSAGE_SELF, &endRusage);
    clock_gettime(CLOCK_MONOTONIC_RAW, &endReal);
    printf("\tUSER:\t %10ldus\t",     (long) ((endRusage.ru_utime.tv_sec - startRusage.ru_utime.tv_sec) * 1000000 + (endRusage.ru_utime.tv_usec - startRusage.ru_utime.tv_usec)));
    printf("\tSYSTEM:\t %10ldus\t",   (long) ((endRusage.ru_stime.tv_sec - startRusage.ru_stime.tv_sec) * 1000000 + (endRusage.ru_stime.tv_usec - startRusage.ru_stime.tv_usec)));
    printf("REAL:\t %10ldus  ",       (long) ((endReal.tv_sec - startReal.tv_sec) * 1000000000 + (endReal.tv_nsec - startReal.tv_nsec)) / 1000);
    printf("|--- %s\n", description);
    resetTimer();
}


char* operationToString(Operation op, FunctionsLibrary fn_l) {
  switch (op) {
    case GENERATE:
      return "Generation time";
    case SHUFFLE:
      if (fn_l == LIB)
        return "Lib shuffle time";
      else if (fn_l == SYS)
        return "Sys shuffle time";
    case SORT:
      if (fn_l == LIB)
        return "Lib sort time";
      else if (fn_l == SYS)
        return "Sys sort time";
  }
  return "*** time";
}

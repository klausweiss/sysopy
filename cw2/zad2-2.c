#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "inc/filesinfo.h"

int printHelp();
void parseArgumentsOrExit(int, char* [], char**, int*);

char* execName;

int main (int argc, char *argv[]) {
    execName = argv[0];

    char* root = NULL;
    int maxSize = 0;
    parseArgumentsOrExit(argc - 1, argv + 1, &root, &maxSize);

    printDirNftw(root, maxSize);

    return 0;
}

int printHelp() {
  printf("  Usage: %s PATH MAX_SIZE\n", execName);
  return 10;
}

void parseArgumentsOrExit(int argc, char *argv[], char** root, int* maxSize) {
    /*
     * Negative error codes do not print the help.
     * Positive error codes do.
     */

    /*
     * number of argument:
     *  2
     */
    if (argc != 2)
        exit(printHelp());
    
    /*
     * input/output file
     */

    struct stat buf;
    if (stat(argv[0], &buf) != 0) {
        printf("Specified file (%s) does not exist.\n", argv[0]);
        exit(-1);
    }

    *root = argv[0];

    *maxSize = atoi(argv[1]);

    if (*maxSize <= 0) {
        printf("Pass a valid positive number (%s).\n", argv[1]);
        exit(-2);
    }
}

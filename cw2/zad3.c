#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include "inc/types.h"
#include "inc/filelocks.h"

int printHelp();
void parseArgumentsOrExit(int, char* [], char**);

FlockOperation parseCommand(char*);
void displayHelp();
void displayResult(int);

char* execName;

int main (int argc, char *argv[]) {
  execName = argv[0];
  char command[32];
  FlockOperation operation;

  char* file = NULL;
  parseArgumentsOrExit(argc - 1, argv + 1, &file);
  int selectedChar;
  char replacementChar;

  int fd = fileno(fopen(file, "rw+"));
  
  displayHelp();
  while (1) {
    printf("flock> ");
    scanf("%s", command);
    operation = parseCommand(command);
    switch (operation) {
      case SET_WRITE_LOCK:
        scanf("%d", &selectedChar);
        displayResult(setWriteLock(fd, selectedChar));
        break;
      case SET_WRITE_LOCK_2:
        scanf("%d", &selectedChar);
        displayResult(setBlockingWriteLock(fd, selectedChar));
        break;
      case SET_READ_LOCK:
        scanf("%d", &selectedChar);
        displayResult(setReadLock(fd, selectedChar));
        break;
      case SET_READ_LOCK_2:
        scanf("%d", &selectedChar);
        displayResult(setBlockingReadLock(fd, selectedChar));
        break;
      case LIST_LOCKS:
        listLocks(fd);
        break;
      case FREE_LOCK:
        scanf("%d", &selectedChar);
        displayResult(freeLock(fd, selectedChar));
        break;
      case READ_CHAR:
        scanf("%d", &selectedChar);
        displayResult(readFileChar(fd, selectedChar));
        break;
      case WRITE_CHAR:
        scanf("%d %c", &selectedChar, &replacementChar);
        displayResult(replaceFileChar(fd, selectedChar, replacementChar));
        break;
      case QUIT:
        exit(0);
      default:
        displayHelp();
        break;
    }
  }

  return 0;
}

void displayResult(int result) {
  printf("\033[1m");
  if (result < 0) {
    printf("\033[0;31m");
    printf("Selected character is locked by a different process");
  } else {
    printf("\033[0;32m");
    printf("ok");
  }
  printf("\n\033[0m");
}

FlockOperation parseCommand(char* command) {
  if (strcmp(command, "setwritelock") == 0 || strcmp(command, "swl") == 0)
    return SET_WRITE_LOCK;
  if (strcmp(command, "setreadlock") == 0 || strcmp(command, "srl") == 0)
    return SET_READ_LOCK;
  if (strcmp(command, "setwritelock2") == 0 || strcmp(command, "swl2") == 0)
    return SET_WRITE_LOCK_2;
  if (strcmp(command, "setreadlock2") == 0 || strcmp(command, "srl2") == 0)
    return SET_READ_LOCK_2;
  if (strcmp(command, "listlocks") == 0 || strcmp(command, "ls") == 0)
    return LIST_LOCKS;
  if (strcmp(command, "freelock") == 0 || strcmp(command, "free") == 0|| strcmp(command, "fl") == 0)
    return FREE_LOCK;
  if (strcmp(command, "read") == 0 || strcmp(command, "r") == 0)
    return READ_CHAR;
  if (strcmp(command, "write") == 0 || strcmp(command, "w") == 0)
    return WRITE_CHAR;
  if (strcmp(command, "quit") == 0 || strcmp(command, "q") == 0 || strcmp(command, "exit") == 0)
    return QUIT;
  return NO_OP;
}

int printHelp() {
  printf("  Usage: %s PATH\n", execName);
  return 10;
}

void parseArgumentsOrExit(int argc, char *argv[], char** file) {
    /*
     * Negative error codes do not print the help.
     * Positive error codes do.
     */

    /*
     * number of argument:
     *  1
     */
    if (argc != 1)
        exit(printHelp());
    
    /*
     * input/output file
     */

    struct stat buf;
    if (stat(argv[0], &buf) != 0) {
        printf("Specified file (%s) does not exist.\n", argv[0]);
        exit(-1);
    }

    *file = argv[0];
}

void displayHelp() {
  printf("\033[1m");
  printf("%26s - %s\n", "setwritelock[2] / swl[2]", "set write lock to selected char");
  printf("%26s - %s\n", "setreadlock[2] / srl[2]", "set read lock to selected char");
  printf("%26s - %s\n", "listlocks / ls", "list locked file characters");
  printf("%26s - %s\n", "freelock / free / fl", "free selected lock");
  printf("%26s - %s\n", "read / r", "read selected char");
  printf("%26s - %s\n", "write / w", "replace selected char");
  printf("%26s - %s\n", "quit / exit / q", "quit");
  printf("\033[0m");
}

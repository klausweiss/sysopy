#ifndef _addressbook_types
#define _addressbook_types

// zad1

typedef enum { SYS, LIB } FunctionsLibrary;
typedef enum { GENERATE, SHUFFLE, SORT } Operation;

typedef union {
    int intFile;
    FILE* FILEfile;
} fileType;

typedef struct {
    fileType file;
    void (*func)();
} fileFunction;

// zad3

typedef enum {
    SET_READ_LOCK,
    SET_WRITE_LOCK,
    SET_READ_LOCK_2,
    SET_WRITE_LOCK_2,
    LIST_LOCKS,
    FREE_LOCK,
    READ_CHAR,
    WRITE_CHAR,
    NO_OP,
    QUIT
} FlockOperation;

#endif

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "filelocks.h"

struct flock* getLockInfo(int fd, int selectedChar, int lockType) {
  struct flock* lock = (struct flock*) malloc(sizeof(struct flock));
  lock->l_type = lockType;
  lock->l_whence = SEEK_SET;
  lock->l_start = selectedChar;
  lock->l_len = 1;
  fcntl(fd, F_GETLK, lock);
  return lock;
}

int setWriteLock(int fd, int selectedChar) {
  struct flock lock;
  lock.l_type = F_WRLCK;
  lock.l_whence = SEEK_SET;
  lock.l_start = selectedChar;
  lock.l_len = 1;
  int status = fcntl(fd, F_SETLK, &lock);
  return status;
}

int lockedAt(int fd, int selectedChar, int lockType) {
  struct flock* lock = getLockInfo(fd, selectedChar, lockType);
  if (lockType == F_RDLCK)
    return lock->l_type == F_WRLCK;
  else if (lockType == F_WRLCK)
    return lock->l_type != F_UNLCK;
  else
    return 0;
}

int setBlockingWriteLock(int fd, int selectedChar) {
  struct flock lock;
  lock.l_type = F_WRLCK;
  lock.l_whence = SEEK_SET;
  lock.l_start = selectedChar;
  lock.l_len = 1;
  fcntl(fd, F_SETLKW, &lock);
  return 1;
}

int setReadLock(int fd, int selectedChar) {
  struct flock lock;
  lock.l_type = F_RDLCK;
  lock.l_whence = SEEK_SET;
  lock.l_start = selectedChar;
  lock.l_len = 1;
  int status = fcntl(fd, F_SETLK, &lock);
  return status;
}

int setBlockingReadLock(int fd, int selectedChar) {
  struct flock lock;
  lock.l_type = F_RDLCK;
  lock.l_whence = SEEK_SET;
  lock.l_start = selectedChar;
  lock.l_len = 1;
  fcntl(fd, F_SETLKW, &lock);
  return 1;
}

int getFileSize(int fd) {
  FILE* fp = fdopen(fd, "r");
  fseek(fp, 0L, SEEK_END);
  int size = ftell(fp);
  return size;
}

char getCharAt(int fd, int charPosition) {
  FILE* fp = fdopen(fd, "r");
  fseek(fp, charPosition, SEEK_SET);
  return fgetc(fp);
}

void printLockInfo(int fd, int selectedChar, int lockType) {
  struct flock* lock = getLockInfo(fd, selectedChar, lockType);
  printf("%5s: %5d (PID: %d)\n", (lock->l_type == F_RDLCK ? "READ" : "WRITE"), selectedChar, lock->l_pid);
}

void listLocks(int fd) {
  int size = getFileSize(fd);
  for (int i=0; i<size; i++) {
    if (lockedAt(fd, i, F_WRLCK))
      printLockInfo(fd, i, F_WRLCK);
    else if (lockedAt(fd, i, F_RDLCK))
      printLockInfo(fd, i, F_RDLCK);
  }
}

int freeLock(int fd, int selectedChar) {
  struct flock lock;
  lock.l_type = F_UNLCK;
  lock.l_whence = SEEK_SET;
  lock.l_start = selectedChar;
  lock.l_len = 1;
  return fcntl(fd, F_SETLKW, &lock);
}

int readFileChar(int fd, int selectedChar) {
  if (lockedAt(fd, selectedChar, F_RDLCK))
    return -1;
  else {
    printf("%c\n", getCharAt(fd, selectedChar));
    return 1;
  } 
}

int replaceFileChar(int fd, int selectedChar, char replacementChar) {
  if (lockedAt(fd, selectedChar, F_WRLCK)) 
    return -1;
  else {
    lseek(fd, selectedChar, SEEK_SET);
    write(fd, &replacementChar, 1);
    return 1;
  }
}

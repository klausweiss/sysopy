#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "common.h"
#include "sort.h"

void sortRecordsSys(int file, int fileSize, int recordsSize) {
  unsigned buffer1;
  unsigned buffer2;
  int smallerIndex;
  int greaterIndex;

  for (int i=0; i<fileSize; i++) {
    for (int j=fileSize-1; j>0; j--) {
      greaterIndex = (recordsSize + 1) * j;
      smallerIndex = (recordsSize + 1) * (j - 1);
      lseek(file, smallerIndex, SEEK_SET);
      read(file, &buffer1, 1);
      lseek(file, greaterIndex, SEEK_SET);
      read(file, &buffer2, 1);
      if (buffer1 > buffer2)
        swapLinesSys(file, recordsSize, smallerIndex, greaterIndex);
    }
  }
}

void sortRecordsLib(FILE* file, int fileSize, int recordsSize) {
  unsigned buffer1;
  unsigned buffer2;
  int smallerIndex;
  int greaterIndex;

  for (int i=0; i<fileSize; i++) {
    for (int j=fileSize-1; j>0; j--) {
      greaterIndex = (recordsSize + 1) * j;
      smallerIndex = (recordsSize + 1) * (j - 1);
      fseek(file, smallerIndex, SEEK_SET);
      buffer1 = fgetc(file);
      fseek(file, greaterIndex, SEEK_SET);
      buffer2 = fgetc(file);
      if (buffer1 > buffer2)
        swapLinesLib(file, recordsSize, smallerIndex, greaterIndex);
    }
  }
}

#include <stdio.h>
#include <stdlib.h>
#include "common.h"
#include "shuffle.h"

void shuffleRecords(fileFunction* ffPair, int fileSize, int recordsSize) {
  int j;
  int index1;
  int index2;
  for (int i=1; i<fileSize; i++) {
    j = rand() % i;
    index1 = (recordsSize + 1) * i;
    index2 = (recordsSize + 1) * j;
    (*ffPair->func)(ffPair->file, recordsSize, index1, index2);
  }
}

void shuffleRecordsSys(int file, int fileSize, int recordsSize) {
  fileFunction* ffPair = (fileFunction*) malloc(sizeof(fileFunction));
  ffPair->file.intFile = file;
  ffPair->func = swapLinesSys;
  shuffleRecords(ffPair, fileSize, recordsSize);
}

void shuffleRecordsLib(FILE* file, int fileSize, int recordsSize) {
  fileFunction* ffPair = (fileFunction*) malloc(sizeof(fileFunction));
  ffPair->file.FILEfile = file;
  ffPair->func = swapLinesLib;
  shuffleRecords(ffPair, fileSize, recordsSize);
}

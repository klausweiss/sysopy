#include <stdio.h>
#include <stdlib.h>
#include "generate.h"

void generateRecordFileOrExit(FILE* file, int fileSize, int recordsSize) {
    if (file == NULL)
        exit(-4);

    FILE* devrandom = fopen("/dev/urandom", "r");
    char* buffer = (char*) malloc(sizeof(char) * (recordsSize + 1));
    for (int i=0; i<fileSize; i++) {
        fread(buffer, 1, recordsSize, devrandom);
        fwrite(buffer, 1, recordsSize, file);
        fwrite("\n", 1, 1, file);
    }
}

#ifndef _FILE_LOCKS_LIB
#define _FILE_LOCKS_LIB

int setWriteLock(int, int);
int setBlockingWriteLock(int, int);
int setReadLock(int, int);
int setBlockingReadLock(int, int);
void listLocks(int);
int freeLock(int, int);
int readFileChar(int, int);
int replaceFileChar(int, int, char);

#endif

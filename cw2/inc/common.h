#include <stdio.h>

#ifndef _common_functions
#define _common_functions

void swapLinesLib(FILE*, int, int, int);
void swapLinesSys(int, int, int, int);

#endif

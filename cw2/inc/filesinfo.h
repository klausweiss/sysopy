#include <dirent.h>
#include <stdio.h>
#include "types.h"

#ifndef _lib_filesinfo
#define _lib_filesinfo

void printDir(char*, int);
void printDirNftw(char*, int);

#endif

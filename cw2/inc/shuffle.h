#include <stdio.h>
#include "types.h"

#ifndef _SHUFFLE_LIB
#define _SHUFFLE_LIB

void shuffleRecordsSys(int, int, int);
void shuffleRecordsLib(FILE*, int, int);

#endif

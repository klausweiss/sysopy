#include <stdlib.h>
#include <unistd.h>
#include "common.h"

void swapLinesLib(FILE* file, int recordsSize, int smallerIndex, int greaterIndex) {
  char* buffer1 = (char*) malloc(sizeof(char) * (recordsSize + 1));
  char* buffer2 = (char*) malloc(sizeof(char) * (recordsSize + 1));

  fseek(file, smallerIndex, SEEK_SET);
  fgets(buffer1, (recordsSize + 2), file);
  fseek(file, greaterIndex, SEEK_SET);
  fgets(buffer2, (recordsSize + 2), file);

  fseek(file, greaterIndex, SEEK_SET);
  fputs(buffer1, file);
  fseek(file, smallerIndex, SEEK_SET);
  fputs(buffer2, file);

  free(buffer1);
  free(buffer2);
}

void swapLinesSys(int file, int recordsSize, int smallerIndex, int greaterIndex) {
  char* buffer1 = (char*) malloc(sizeof(char) * recordsSize);
  char* buffer2 = (char*) malloc(sizeof(char) * recordsSize);

  lseek(file, smallerIndex, SEEK_SET);
  read(file, buffer1, recordsSize);
  lseek(file, greaterIndex, SEEK_SET);
  read(file, buffer2, recordsSize);

  lseek(file, greaterIndex, SEEK_SET);
  write(file, buffer1, recordsSize);
  lseek(file, smallerIndex, SEEK_SET);
  write(file, buffer2, recordsSize);

  free(buffer1);
  free(buffer2);
}

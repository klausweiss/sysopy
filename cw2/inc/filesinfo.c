#define _XOPEN_SOURCE 500
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <ftw.h>
#include "filesinfo.h"

int notHereNorParent(char*, char*);
void printFileInfo(const char*);
int fileSmallerThan(const char*, int);
char* privilegesToString(mode_t);
void dateToString(time_t*, char*);

void printDirRecursively(char* path, char* name, int maxSize) {
  DIR* dir = opendir(path);
  struct dirent* file = NULL;
  
  char* newPath = (char*) malloc(sizeof(char)*1024);
  while ((file = readdir(dir))) {
    strcpy(newPath, path);
    strcat(newPath, "/");
    strcat(newPath, file->d_name);
    if(file->d_type == DT_DIR) {
      if(notHereNorParent(name, file->d_name)) {
        printDirRecursively(newPath, file->d_name, maxSize);
      }
    } else if (file->d_type == DT_REG){
      if (fileSmallerThan(newPath, maxSize))
        printFileInfo(newPath);
    }
  }
  free(newPath);
}

void printDir(char* path, int maxSize) {
  printDirRecursively(path, ".", maxSize);
}

int fileSmallerThan(const char* path, int maxSize) {
  struct stat buf;
  stat(path, &buf);
  return buf.st_size < maxSize;
}

void printFileInfo(const char* path) {
  struct stat buf;
  stat(path, &buf);
  char date[80];
  dateToString(&buf.st_mtime, date);
  char* privileges = privilegesToString(buf.st_mode);
  char absolutePath[1000];
  realpath(path, absolutePath);
  printf("%8zu B | %s | %s | %s\n", buf.st_size, privileges, date, absolutePath);
  free(privileges);
}

int notHereNorParent(char* path, char* name) {
  return strcmp(".", name) * strcmp("..", name) * strcmp(path, name) != 0;
}

char* privilegesToString(mode_t privileges) {
  char* buffer = (char*) malloc(10);
  snprintf(buffer, 10, "%s%s%s%s%s%s%s%s%s", (privileges & S_IRUSR ? "r" : "-"),
                                             (privileges & S_IWUSR ? "w" : "-"),
                                             (privileges & S_IXUSR ? "x" : "-"),
                                             (privileges & S_IRGRP ? "r" : "-"),
                                             (privileges & S_IWGRP ? "w" : "-"),
                                             (privileges & S_IXGRP ? "x" : "-"),
                                             (privileges & S_IROTH ? "r" : "-"),
                                             (privileges & S_IWOTH ? "w" : "-"),
                                             (privileges & S_IXOTH ? "x" : "-")
                                             );
  return buffer;
}

void dateToString(time_t* dateTime, char* buffer) {
  struct tm timeinfo;
  localtime_r(dateTime, &timeinfo);
  strftime(buffer, 80, "%Y-%m-%d %H:%M:%S", &timeinfo);
}


/// VARIANT 2

int handleNftwDirectory(const char* fpath, const struct stat* sb, int typeflag, struct FTW* ftwbuf) {
  // The black magic here saves maxSize in this function context.
  // It should be called with parameters NULL, NULL, maxSize, NULL before passing
  // as nftw callback.
  static int maxSize; 
  if (fpath == NULL && sb == NULL && ftwbuf == NULL) {
    maxSize = typeflag;
    return 0;
  }

  if (fileSmallerThan(fpath, maxSize)) 
    printFileInfo(fpath);
  return 0;
}

void printDirNftw(char* path, int maxSize) {
  handleNftwDirectory(NULL, NULL, maxSize, NULL);
  nftw(path, *handleNftwDirectory, 0, 0);
}
